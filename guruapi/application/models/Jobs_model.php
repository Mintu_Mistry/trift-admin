<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "jobs";  
	}
	/* CRUD Operations for jobs*/
///////////////////////////////////////////////////////////////////////////////////////////////////	
	function create($jobsarray)
	{
		$this->db->insert($this->tablename,$jobsarray);
		//echo $this->db->last_query();
		return true;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////
	function read()
	{	
		$this->db->select('jobid');
		$this->db->from($this->tablename);
		$query=$this->db->get();
		return $query->result_array();
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
}


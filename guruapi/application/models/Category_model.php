<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "category";  
	}
	/* CRUD Operations for category*/
///////////////////////////////////////////////////////////////////////////////////////////////////
	function select(){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$query=$this->db->get();
		return $query->result_array();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	function status_update($category_id,$status){
		if($status=='1'){
                  $status='0';
              }else{
                  $status='1';
              }
		$this->db->where('categoryid',$category_id);
		$this->db->update($this->tablename,array('category_status'=>$status));
		return true;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////
function update($category_id,$categorydata){
	$this->db->where('categoryid',$category_id);
	$this->db->update($this->tablename,$categorydata);
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
function select_title($categoryid){
		$this->db->select('category.*');
		$this->db->from($this->tablename);
		$this->db->where('categoryid',$categoryid);
		$query=$this->db->get();
		return $query->result_array();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	
}
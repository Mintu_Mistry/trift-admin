<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "messages";  
	}
	/* CRUD Operations for messages*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function create($messagearray)
	{
		$this->db->insert($this->tablename,$messagearray);
		//echo $this->db->last_query();
		return true;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////	
	function select_groupbysenderid($jobid="")
	{
		$this->db->select('senderid,sender_screen_name,sender_type,jobid,jobtitle');
		$this->db->from($this->tablename);
		$this->db->group_by('jobid');
		$this->db->where('sender_type','Employer');
		if(!empty($jobid)){
			$this->db->where('jobid',$jobid);
		}
		$query=$this->db->get();
		return $query->result_array();
	}
///////////////////////////////////////////////////////////////////////////////////////////////////	
	// function select_message_sendername()
	// {

	// }
/////////////////////////////////////////////////////////////////////////////////////////////////////	
	function select_all($jobid)
	{
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('jobid',$jobid);
		$this->db->order_by('dateposted','ASC');
		$query=$this->db->get();
		return $query->result_array();
	}
///////////////////////////////////////////////////////////////////////////////////////////////////	
	function select_message_rows()
	{
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('dateposted');
		$query=$this->db->get();
		return $query->num_rows();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	function insert($send_messagearray){
		$this->db->insert($this->tablename,$send_messagearray);
		return true;
		
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
}
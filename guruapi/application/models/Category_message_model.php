<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_message_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "category_message";  
	}
	/* CRUD Operations for category_message*/
///////////////////////////////////////////////////////////////////////////////////////////////////////
	function create($category_message)
	{
		$this->db->insert($this->tablename,$category_message);
		return true;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////
	function select($categoryid){
		$this->db->select('messages.*');
		$this->db->from($this->tablename);
		if(!empty($categoryid)){
		$this->db->where('categoryid',$categoryid);
		$this->db->join('messages','messages.messageid=category_message.messageid');
    	}
    	$query=$this->db->get();
    	return $query->result_array();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
function selectcount($messageid){
	$this->db->select('*');
	$this->db->from($this->tablename);
	$this->db->where('messageid',$messageid);
    $num = $this->db->count_all_results();
    if($num==1){
    	return true;
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
function update($messageid,$category_message){
	$this->db->where('messageid',$messageid);
	$this->db->update($this->tablename,$category_message);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
}
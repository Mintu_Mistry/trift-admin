<?php if(!empty($this->session->flashdata('success')))
  { ?>  
    <div class="alert alert-success alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('success');?>
    </div>
    </div>
<?php }?>
<?php if(!empty($this->session->flashdata('error')))
  { ?>  
    <div class="alert alert-danger alert-dismissible ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('error');?>
    </div>
    </div>
<?php }?>
<section class="section">
          <div class="row ">
            <div class="col-xl-6 col-lg-8 col-md-6 col-sm-6 col-xs-12">
            	<a href="<?php echo base_url();?>guruapi/Admin/allmessages" style="text-decoration: none;">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15">All Messages</h5>
                          <h2 class="mb-3 font-18"></h2>
                          <p class="mb-0"><span class="col-green"></span></p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0" >
                        <div class="banner-img" >
                          <img src="<?php echo base_url();?>guruapi/assets/img/banner/5.jpg" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-xl-6 col-lg-8 col-md-6 col-sm-6 col-xs-12">
            	<a href="<?php echo base_url();?>guruapi/Admin/proposal" style="text-decoration: none;">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15"> Proposal </h5>
                          <h2 class="mb-3 font-18"></h2>
                          <p class="mb-0"><span class="col-orange"></span> </p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                        <div class="banner-img">
                          <img src="<?php echo base_url();?>guruapi/assets/img/banner/7.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<a href="<?php echo base_url();?>guruapi/Admin/category" style="text-decoration: none;">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15">Category</h5>
                          <h2 class="mb-3 font-18"></h2>
                          <p class="mb-0"><span class="col-green"></span>
                            </p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                        <div class="banner-img">
                          <img src="<?php echo base_url();?>guruapi/assets/img/banner/11.svg" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </a>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<a href="<?php echo base_url();?>guruapi/Admin/allmessages" style="text-decoration: none;">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15">Revenue</h5>
                          <h2 class="mb-3 font-18"></h2>
                          <p class="mb-0"><span class="col-green"></span> </p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                        <div class="banner-img">
                          <img src="<?php echo base_url();?>guruapi/assets/img/banner/9.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </a>
          </div>
<!------------------------------------------------------------------------------------------------------------>
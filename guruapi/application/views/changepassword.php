 <?php if(!empty($this->session->flashdata('success')))
    { ?>  
    <div class="alert alert-success alert-dismissible  ">
                      <div class="alert-body">
                        <?php echo $this->session->flashdata('success');?>
                      </div>
                    </div>
       <?php }?>
<section class="section">
      <div class="container mt--50">
        <div class="row">
          <div class="col-2"></div>
          <div class="col-8 ">
            <div class="card card-primary">
              <div class="card-header">
                <h4>Reset Password</h4>
              </div>
              <div class="card-body">
                <p class="text-muted"></p>
                <form method="POST" action="<?php echo base_url();?>guruapi/Admin/changepassword_submit">
                  <div class="row">
                    
                <div class="col-12">
                  <div class="form-group">
                    <label for="password">Old Password</label>
                    <input type="hidden" name="admin_id" value="<?php echo $admin['admin_id'];?>">
                    <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator"
                      name="password"  required>
                    <div id="pwindicator" class="pwindicator">
                      <div class="bar"></div>
                      <div class="label"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                    <div class="col-12">
                  <div class="form-group">
                    <label for="password">New Password</label>
                    <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator"
                      name="npassword"  required>
                    <div id="pwindicator" class="pwindicator">
                      <div class="bar"></div>
                      <div class="label"></div>
                    </div>
                  </div>
                </div>
              </div>
                <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label for="password-confirm">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="cpassword"
                       required>
                  </div>
                </div>
              </div>
                  <div class="text-right card-footer ">
                    <button type="submit" class="btn btn-primary " name="submit">
                      Reset Password
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  
<!-- <?php  //echo $this->input->get('senderid');?> -->
<?php if(!empty($this->session->flashdata('success')))
  { ?>  
    <div class="alert alert-success alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('success');?>
    </div>
    </div>
<?php }?>
<?php if(!empty($this->session->flashdata('error')))
  { ?>  
    <div class="alert alert-danger alert-dismissible ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('error');?>
    </div>
    </div>
<?php }?>
<section class="section">
          <div class="section-body ">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                  <div class="chat">
                      <!-- <img src="assets/img/users/user-1.png" alt="avatar">
                       --><div class="chat-about ">
                        <?php foreach ($senderdetail as $key => $values) {
                          ?>

                        <div class="chat-with sendername"><?php echo $values['sender_screen_name']?></div>
                        <div class="chat-num-messages sendertype"><?php echo $values['sender_type']?></div>
                        <div class="chat-about jobtitle"><b><?php echo $values['jobtitle']?></b></div>
                        

                      <?php }?>
                      </div>
                    
                  
                  <div class="chat-box  " id="mychatbox">
                    
                    <div class="card-body chat-scroll char-content " style="max-height: 400px;
                      overflow-y: scroll; overflow-x: hidden;">
                   <?php
                 foreach ($allmessages as $key => $values) {
                  ?>
                  <li>  
                    <?php echo $values['sender_screen_name']?>
                     <p class="chat-message" >
                    <?php  echo $values['message'];?>
                    <?php echo $values['filename'];?>
                    <?php echo $values['fileurl'];?>
<!-----========================category button===========================------------------------->                    <form method="post" id="categoryform">
                    <div class="btn-group mb-2 popoverbtn">
                     <div class="form-group">
                      <select class="form-control select2 selectcategory" name="selectcategory" id="selectoption" onchange="categorychange(<?php echo $values['messageid'];?>,this)">
                        <option value="">Select Category</option>
                        <?php foreach ($category as $key => $value) {                        
                          ?>
                        <option name="category" id="categoryoption" value="<?php echo $value['categoryid'];?>"><?php echo $value['category_title'];?></option>
                        <?php }?>
                         
                      </select>
                    </div>                    
                      </div>  
                      </form>                                                        
<!-----===================================================================------------------------->                  </p>
                  <div class="date"><?php $time = date("d-m-Y H:i:s",$values['dateposted']);
                     echo @$time;?></div>
                  </li>
                  <?php 
                  }
                  ?>

                    </div>
                    <div class="card-footer chat-form">
                      <form id="chat-form" method="post" action="<?php echo base_url();?>guruapi/Admin/send_message" >
                        <input type="text" class="form-control" name="message" placeholder="Type a message">
                        <button class="btn btn-primary" type="submit" name="submit">
                          <i class="far fa-paper-plane"></i>
                        </button>
                      </form>
                    </div>
                    
                  </div>
                  </div>
                  </div>
                </div>
              
              </div>
            </div>
          </div>
        </section>
<!------=======================================================================================--->


<section class="section">
<div class="section-body">
  
  <!-- <div class="col-12 col-md-3 col-lg-4 ">
                <div class="card " >
                  <center>
                  <div class="card-body ">
                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#allmessage">Send Message</button>
                  </div>
                </center>
                </div>
              </div> -->
   <!-- <div class="col-12 col-md-3 col-lg-4">
                <div class="card">
                  <center>
                  <div class="card-body">
                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#category1">Send Message</button>
                  </div></center>
                </div>
              </div> -->
  <!-- <div class="col-12 col-md-3 col-lg-4">
                <div class="card">
                  <center>
                  <div class="card-body">
                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#category2">Send Message</button>
                  </div></center>
                </div>
              </div> -->
          
<div class="row">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-12">
                <div class="card card-primary">
                  <div class="body">
                    <div id="plist" class="people-list">
                     <div class="card-header">
                    <i data-feather="message-circle" class="text-info"></i> <h4 class="text-info">All Message</h4>
                  </div>
                          <div class="m-b-20">
                        <div id="chat-scroll">

                          <ul class="chat-list list-unstyled m-b-0">  
                          <?php
                          
                           foreach ($senderdetail as $key => $values) {
                          
                              ?>  
                          <li class="media">
                        <div class="media-body">
                          
                          <h6 class="media-title"><a href="<?php echo base_url();?>guruapi/Admin/user_message?jobid=<?php echo @$values['jobid'];?>">
                            <?php echo @$values['sender_screen_name'];?>
                             </a>
                          </h6>
                          <div class="text-small text-muted"><?php echo @$values['sender_type'];?></div>
                        </div>
                      <!-- <div class="name"><?php echo @$values['jobid']?></div> -->
                              <!-- <img src="<?php echo base_url();?>guruapi/assets/img/users/user-4.png" alt="avatar"> -->                              
                            </li>
                         <?php }?>
                     
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
</div>
</section>
  <!----------------------------------------------------------------------------------------------------------->
        
          
<!----------------------------------------------------------------------------------------------------->
<!--------------------------------------- All Message Pop Up --------------------------------------- -->
        <div class="modal fade" id="allmessage" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Send Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>guruapi/Admin/sendmessage_submit">
                  <div class="form-group">
                    
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-envelope"></i>
                        </div>
                      </div>
                      <input type="text" class="form-control" placeholder="Write your Message Here" name="message">
                    </div>
                     <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-file-upload"></i>
                        </div>
                      </div>
                      <input  type="file" class="form-control "  name="files">
                    </div>
                      
                  <button type="submit" class="btn btn-primary m-t-15 waves-effect">Send Now</button>
                  <button type="submit" class="btn btn-primary m-t-15 waves-effect">Schedule Message</button>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
<!------------------------------------------------------------------------------------------------->
<!--------------------------------------- category1 Pop Up --------------------------------------- -->
        <div class="modal fade" id="category1" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Category1 Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="" method="post" enctype="multipart/form-data" >
                  <div class="form-group">
                    
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-envelope"></i>
                        </div>
                      </div>
                      <input type="text" class="form-control" placeholder="Write your Message Here" name="message">
                    </div>
                     <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-file-upload"></i>
                        </div>
                      </div>
                      <input  type="file" class="form-control "  name="files">
                    </div>
                      <center>
                  <button type="button" class="btn btn-primary m-t-15 waves-effect">Send Now</button>
                  <button type="button" class="btn btn-primary m-t-15 waves-effect">Schedule Message</button>
                </center>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!----------------------------------------------------------------------------------------------------->
<!--------------------------------------- category2 Pop Up --------------------------------------- -->
        <div class="modal fade" id="category2" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Category2 Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-envelope"></i>
                        </div>
                      </div>
                      <input type="text" class="form-control" placeholder="Write your Message Here" name="message">
                    </div>
                     <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-file-upload"></i>
                        </div>
                      </div>
                      <input  type="file" class="form-control "  name="files">
                    </div>
                      
                  <button type="button" class="btn btn-primary m-t-15 waves-effect">Send Now</button>
                  <button type="button" class="btn btn-primary m-t-15 waves-effect">Schedule Message</button>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      
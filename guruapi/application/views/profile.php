<?php if(!empty($this->session->flashdata('success')))
  { ?>  
    <div class="alert alert-success alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('success');?>
    </div>
    </div>
<?php }?>
<?php if(!empty($this->session->flashdata('error')))
  { ?>  
    <div class="alert alert-danger alert-dismissible ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('error');?>
    </div>
    </div>
<?php }?>

<section class="section">
          <div class="section-body">
            <div class="row ">
              <div class="col-12 col-md-12 col-lg-1"></div>
              <div class="col-12 col-md-12 col-lg-10">
                <div class="card card-primary">
                  <div class="card-header text-warning">
                    <i  data-feather="user"></i> <h4 class="text-warning"> Profile</h4>
                  </div>
                  <div class="padding-20">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab"
                          aria-selected="true">About</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#edit" role="tab"
                          aria-selected="false">Edit </a>
                      </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                      <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                        <div class="row">
                          <div class="col-md-4 col-8 col- b-r">
                          
                      <img alt="image" src="<?php echo base_url();?>guruapi/assets/image/<?php echo $admin['admin_photo'];?>" class="admin1 ">
                      
                    </div>

                          <div class="col-md-4 col-6 b-r">
                            <strong>Full Name</strong>
                            <br>
                            <p class="text-muted"><?php echo $admin['admin_name'];?></p>
                          </div>
                          
                          <div class="col-md-4 col-6 b-r">
                            <strong>Email</strong>
                            <br>
                            <p class="text-muted"><?php echo $admin['admin_email'];?></p>
                          </div>
                          
                        </div>
                       
                      </div>
                      <div class="tab-pane fade" id="edit" role="tabpanel" aria-labelledby="profile-tab2">
                        <form method="post" class="" action="<?php echo base_url();?>guruapi/Admin/profile_edit" enctype="multipart/form-data">
                          <div class="card-header">
                            <h4>Edit Profile</h4>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <div class="form-group col-md-6 col-12">
                                <label>First Name</label>
                                <input type="text" class="form-control" value="<?php echo $admin['admin_name'];?>" name="admin_name" required="">
                                
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Email</label>
                                <input type="email" class="form-control" value="<?php echo $admin['admin_email'];?>" name="admin_email" required="">
                                
                              </div>
                              
                            </div>
                            <div class="row">
                              
                      <input type="file" class="form-control" name="adminphoto" value="<?php echo $admin['admin_photo'];?>" >
                    
                            </div>
                            
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary" name="submit" type="submit">Save Changes</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
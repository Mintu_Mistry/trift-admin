<?php if(!empty($this->session->flashdata('success'))) { ?>  
    <div class="alert alert-success alert-dismissible">
    <div class="alert-body">
    <?php echo $this->session->flashdata('success');?>
    </div>
    </div>
<?php }?>
<?php if(!empty($this->session->flashdata('error')))
  { ?>  
    <div class="alert alert-danger alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('error');?>
    </div>
    </div>
<?php }?>
<section class="section">
  <div class="section-body">
    <div class="row">
      <div class="col-12 ">
        <div class="card card-primary">
          <div class="card-header">
            <i data-feather="list" class="text-warning"></i> <h4 class="text-warning"> All Category</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-2">
                <thead>
                  <tr>
                    <th class="text-center">
                      #
                    </th>
                    
                  <th class="text-center">Category Title</th>
                  <th class="text-center">Status</th>
                  <th >Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <?php 
                  $i=0;
                  foreach ($category as $key => $value) {
                    $i++;
                    ?>
                    <td class="text-center"><?php echo $i;?></td>
                  <td class="text-center"><a href="<?php echo base_url();?>/guruapi/Admin/categorized_messages/<?php echo $value['categoryid'];?>"><?php echo $value['category_title'];?></td>
                  <td class="text-center">
                    <?php if($value['category_status']==1){
                      ?>
                      <a href="<?php echo base_url();?>guruapi/Admin/category_status/<?php echo $value['categoryid'];?>/<?php echo $value['category_status'];?>">
                      <div class="badge badge-success ">Active</div></a>
                      <?php 
                    }elseif($value['category_status']==0){
                      ?>
                      <a href="<?php echo base_url();?>guruapi/Admin/category_status/<?php echo $value['categoryid'];?>/<?php echo $value['category_status'];?>">
                      <div class="badge badge-danger">InActive</div></a>
                      <?php 
                    }
                    ?>
                  </td>
                  <td>
                     <a class="btn btn-primary btn-action mr-1" data-toggle="modal"  type="button" data-target="#editmodal<?php echo $value['categoryid'];?>" ><i
                        class="fas fa-pencil-alt"></i></a>
                    <!-- <a class="btn btn-danger btn-action" data-toggle="tooltip" id="swal-6" title="Delete"
                      data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?"
                      data-confirm-yes="alert('Deleted')"><i class="fas fa-trash"></i></a> -->
                  </td>
                </tr>
                <?php }?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
        
 <!------------======================category edit---------------------------------------------->
 
 <!-- Modal with form -->
 <?php foreach ($categoryedit as $key => $value) {
   ?>
  <div class="modal fade" id="editmodal<?php echo $value['categoryid'];?>" tabindex="-1" role="dialog" aria-labelledby="formModal"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="formModal">Update Category</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="" method="post" action="<?php echo base_url();?>guruapi/Admin/category_edit/<?php echo $value['categoryid'];?>">
            <div class="form-group">
              <label>Category Title</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    <i class="fas fa-stream"></i>
                  </div>
                </div>
                <!-- <input type="hidden" class="form-control" name="categoryid" value="<?php echo $value['categoryid'];?>"> -->
                <input type="text" class="form-control" placeholder="Category Title" name="category_title" value="<?php echo $value['category_title'];?>">
              </div>
            </div>
            <div class="form-group">
              <label>Status</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    <i class="fas fa-lightbulb"></i>
                  </div>
                </div>
                <select name="categorystatus" class="form-control " placeholder="Select Status" value="<?php echo $value['category_status'];?>">
                 <optgroup label="Select Status"> 
                  <option name="categorystatus" <?php echo $value['category_status']==1 ? 'selected':'';?> value="1">Active</option>
                  <option name="categorystatus"<?php echo $value['category_status']==0 ? 'selected':'';?> value="0">InActive</option>
                  </optgroup>
                </select>
              </div>
            </div>
          <center>
            <button type="submit" name="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
          </center>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php }?>
   
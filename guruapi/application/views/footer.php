</div>
      <footer class="main-footer">
        <div class="footer-left">
          
        </div>
        <div class="footer-right">
        </div>
      </footer>
    </div>
  </div>
  <!-- General JS Scripts -->
  <script src="<?php echo base_url();?>guruapi/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo base_url();?>guruapi/assets/bundles/apexcharts/apexcharts.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/page/index.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/custom.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo base_url();?>guruapi/assets/bundles/sweetalert/sweetalert.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/page/sweetalert.js"></script>

  <script src="<?php echo base_url();?>guruapi/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo base_url();?>guruapi/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>guruapi/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/page/datatables.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/page/chat.js"></script>

<!---------------------------Alert faded------------------------------------------------------->
    <script>
window.setTimeout(function() {
  $(".alert-success").fadeTo(1500, 0).slideUp(800, function(){
      $(this).remove();
  });
}, 2500);

window.setTimeout(function() {
  $(".alert-danger").fadeTo(1500, 0).slideUp(800, function(){
      $(this).remove();
  });
}, 2500);
window.setTimeout(function() {
  $(".alert-info").fadeTo(1500, 0).slideUp(800, function(){
      $(this).remove();
  });
}, 2500);

window.setTimeout(function() {
  $(".alert-warning").fadeTo(1500, 0).slideUp(800, function(){
      $(this).remove();
  });
}, 2500);


///////////////////////////////////////////////////////////////////////////////////////////////////
$(".delete_confirm").click(function(event){
  if (confirm("Are you sure you want to delete?")) {
    return true;
  }
  else {
    event.preventDefault();
  }
});
////////////////////////////////////////////////////////////////////////////////////////////////////

$("input").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});
///////////////////////////////////////////////////////////////////////////////////////////////////

function categorychange(messageid,categoryid){
 categoryid = categoryid.value;

 $.post("<?php echo base_url('guruapi/Admin/category_message') ?>",{messageid:messageid,categoryid:categoryid},function(res){
             // location.reload();
    }
  );

}
//////////////////////////////////////////////////////////////////////////////////////////////////////
// function categoryedit(categoryid){
// //alert(categoryid);
//  $.post("<?php echo base_url('guruapi/Admin/category_edit') ?>",{categoryid:categoryid},function(res){
//              // location.reload();
//     }
//   );

// }
//////////////////////////////////////////////////////////////////////////////////////////////////////
</script>

</body>
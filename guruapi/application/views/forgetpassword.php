<!DOCTYPE html>
<html lang="en">


<!-- auth-login.html  21 Nov 2019 03:49:32 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Guru API</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/bundles/bootstrap-social/bootstrap-social.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url();?>guruapi/assets/img/favicon.ico' />
</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
<?php if(!empty($this->session->flashdata('success')))
  { ?>  
    <div class="alert alert-success alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('success');?>
    </div>
    </div>
<?php }?>
<?php if(!empty($this->session->flashdata('error')))
  { ?>  
    <div class="alert alert-danger alert-dismissible ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('error');?>
    </div>
    </div>
<?php }?>


            <div class="card card-primary">
              <div class="card-header">
                <h4>Forgot Password</h4>
              </div>
              <div class="card-body">
                <p class="text-muted">We will send a link to reset your password</p>
                <form method="POST" action="<?php echo base_url();?>guruapi/Admin/forgetpassword_submit">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" tabindex="1" required autofocus>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Forgot Password
                    </button>
                  </div>
                  <center>
                  <a type="button" href="<?php echo base_url();?>guruapi/Admin/login" class="btn btn-success "><i data-feather="log-in"></i> <span>Login</span></a>
                </center>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="<?php echo base_url();?>guruapi/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/custom.js"></script>
</body>


<!-- auth-login.html  21 Nov 2019 03:49:32 GMT -->
</html>
<!---------------------------Alert faded------------------------------------------------------->
    <script>
window.setTimeout(function() {
  $(".alert-success").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);

window.setTimeout(function() {
  $(".alert-danger").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);
window.setTimeout(function() {
  $(".alert-info").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);

window.setTimeout(function() {
  $(".alert-warning").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);



////////////////////////////////////////////////////////////////////////////////////////////////////

$("input").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});
///////////////////////////////////////////////////////////////////////////////////////////////////
</script>
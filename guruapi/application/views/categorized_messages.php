 <?php if(!empty($this->session->flashdata('success')))
  { ?>  
    <div class="alert alert-success alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('success');?>
    </div>
    </div>
<?php }?>
<?php if(!empty($this->session->flashdata('error')))
  { ?>  
    <div class="alert alert-danger alert-dismissible ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('error');?>
    </div>
    </div>
<?php }?>

 <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12 ">
                <div class="card card-primary">
                  <div class="card-header">
                    <i class="fas fa-envelope-open-text  text-dark fa-7x" ></i> <h4 class="text-dark">
                            <?php echo $title[0]['category_title'];?> Messages
                          </h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                          
                          <th class="text-center">Message</th>

                          <th class="text-center">Sender Name</th>
                          <th class="text-center">Job Title</th>

                          </tr>
                        </thead>
                        <tbody>
                          <tr>

                          <?php 
                          $i=0;
                          foreach ($categorized as $key => $value) {
                          $i++; 
                            ?>
                            <td class="text-center"><?php echo $i;?></td>
                          <td class=""><?php echo $value['message'];?></td>
                          <td class="text-center"><?php echo $value['sender_screen_name'];?></td>
                          <td class="text-center"><?php echo $value['jobtitle'];?></td>
                           <td>
                             <!-- <a class="btn btn-primary btn-action mr-1" data-toggle="modal"  type="button" data-target="#editmodal" ><i
                                class="fas fa-pencil-alt"></i></a> -->
                             <!-- <a class="btn btn-danger btn-action" data-toggle="tooltip" id="swal-6" title="Delete"
                              data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?"
                              data-confirm-yes="alert('Deleted')"><i class="fas fa-trash"></i></a>  -->
                          </td> 
                        </tr>
                        <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        
 <!------------======================category edit---------------------------------------------->
 
 <!-- Modal with form -->
        <div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Update Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="" method="post" action="<?php echo base_url();?>guruapi/Admin/category_edit/<?php echo $value['categoryid'];?>">
                  <div class="form-group">
                    <label>Category Title</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-stream"></i>
                        </div>
                      </div>
                      <input type="text" class="form-control" placeholder="Category Title" name="category_title" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-lightbulb"></i>
                        </div>
                      </div>
                      <select name="category" class="form-control" placeholder="Select Status">
                       
                        <option name="category_status" value="1">Active</option>
                        <option name="category_status" value="0">InActive</option>
                        
                      </select>
                    </div>
                  </div>
                <center>
                  <button type="submit" name="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                </center>
                </form>
              </div>
            </div>
          </div>
        </div>       
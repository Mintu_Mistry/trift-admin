<!DOCTYPE html>
<html lang="en">


<!-- auth-login.html  21 Nov 2019 03:49:32 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Guru API</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/bundles/bootstrap-social/bootstrap-social.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url();?>guruapi/assets/img/favicon.ico' />
</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
<?php if(!empty($this->session->flashdata('success')))
  { ?>  
    <div class="alert alert-success alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('success');?>
    </div>
    </div>
<?php }?>
<?php if(!empty($this->session->flashdata('error')))
  { ?>  
    <div class="alert alert-danger alert-dismissible   ">
    <div class="alert-body ">
    <?php echo $this->session->flashdata('error');?>
    </div>
    </div>
<?php }?>
            <div class="card card-primary">
              <div class="card-header">
                <h4>Reset Password</h4>
              </div>
              <div class="card-body">
                <p class="text-muted">Enter Your New Password</p>
              <form method="POST" action="<?php echo base_url();?>guruapi/Admin/resetpassword_submit">
                  <div class="form-group">
                    <label for="password">New Password</label>
                    <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator"
                      name="npassword" tabindex="2" required>
                    <div id="pwindicator" class="pwindicator">
                      <div class="bar"></div>
                      <div class="label"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password-confirm">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="cpassword"
                      tabindex="2" required>
                  </div>
                  <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Reset Password
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="<?php echo base_url();?>guruapi/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo base_url();?>guruapi/assets/js/custom.js"></script>
</body>


<!-- auth-reset-password.html  21 Nov 2019 04:05:02 GMT -->
</html>
<script type="text/javascript">
  $("input").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});
/////////////////////////////////////////////////////////////////////////////////////////////////
window.setTimeout(function() {
  $(".alert-success").fadeTo(1000, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);

window.setTimeout(function() {
  $(".alert-danger").fadeTo(1000, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);
window.setTimeout(function() {
  $(".alert-info").fadeTo(1000, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);

window.setTimeout(function() {
  $(".alert-warning").fadeTo(1000, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 2000);
/////////////////////////////////////////////////////////////////////////////////////////////////

</script>
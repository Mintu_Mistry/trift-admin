<!DOCTYPE html>
<html lang="en">


<!-- index.html  21 Nov 2019 03:44:50 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Guru API</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url();?>guruapi/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url();?>/assets/img/favicon.ico' />
</head>
<style type="text/css">
  .chat-message{
    min-width: 80%;
    padding: 5px;
    margin:5px 10px 0;
    background: #99ccff;
    border-radius: 10px;
    color: #fff;

  }
  
  .jobtitle{
    margin-left: 70%;
    color: #123;
    font-size: 16px;
  }
  .sendertype{
    margin-left: 5px;
    color: #abc;
    font-size: 15px;
  }
  .sendername{
    margin-left: 5px;
    color: #12a;
    font-size: 25px;
  }
  .date{
    margin-left: 80%;
  }
  .popoverbtn{
    margin-left: 80%;
    margin-top:-55px;
  }
  .cardbody{
    max-height: 400px;
    overflow: scroll;
    padding: 10px;
    width: 100%;
  }
  .selectcategory{
    margin-right: 70px;
    min-width: 100%;
  }
  .width300{
    max-width: 300px;
  }
  .admin{
    height: 45px;
    min-width:65px; 
  }
  .admin1{
    height: 100px;
    min-width: 100px;
    margin-left: 20px;
    border-radius: 10px;
   box-shadow: 0 10px 6px -6px #777;
  }
  
</style>
<body>

  <div class="loader"></div>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg "></div>
      <nav class="navbar navbar-expand-lg main-navbar sticky active ">
        <div class="form-inline mr-auto">
          <ul class="navbar-nav mr--10">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg
                  collapse-btn"> <i data-feather="align-justify"></i></a></li>
          </ul>

        </div>
        <ul class="navbar-nav navbar-right">
          
           <li class="dropdown"><a href="#" data-toggle="dropdown"
              class="nav-link dropdown-toggle nav-link-lg nav-link-user"> 
              <img  src="<?php echo base_url();?>guruapi/assets/image/<?php echo $admin['admin_photo'];?> "
                class="user-img-radious-style admin"> <span class="d-sm-none d-lg-inline-block"></span></a>

            <div class="dropdown-menu dropdown-menu-right pullDown">
              <div class="dropdown-title"><b>Hello <?php echo $admin['admin_name'];?></b></div>
              <a href="<?php echo base_url();?>guruapi/Admin/profile" class="dropdown-item has-icon text-warning"> <i class="far
                    fa-user"></i> Profile
              </a> 
               <a href="<?php echo base_url();?>guruapi/Admin/changepassword" class="dropdown-item has-icon text-success"> <i class="fas fa-cog"></i>
                Change Password
              </a>
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url();?>guruapi/Admin/login" class="dropdown-item has-icon text-danger "> <i class="fas fa-sign-out-alt"></i>
                Logout
              </a>
              
            </div>
          </li> 
          
        </ul>
      </nav>
      <!------------------------------------------side bar--------------------------------------------->
      <div class="main-sidebar sidebar-style-2 ">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand ">
            <a href="<?php echo base_url();?>guruapi/Admin"> <img alt="image" src="<?php echo base_url();?>guruapi/assets/img/logo.png" class="header-logo" /> <span
                class="logo-name">Guru API</span>
            </a>
          </div>
          <ul class="sidebar-menu">
            <li class="dropdown active">
              <a href="<?php echo base_url();?>guruapi/Admin" class="nav-link "><i data-feather="monitor"></i><span>Dashboard</span></a>
            </li>
            
             
              
              <li class="dropdown ">
              <a href="<?php echo base_url();?>guruapi/Admin/allmessages" class="nav-link text-info"><i data-feather="message-circle"></i><span>All Message</span></a>
            </li>
                <!-- <li><a class="nav-link" href="widget-data.html">Data Widgets</a></li> -->
              
              <li class="dropdown ">
              <a href="<?php echo base_url();?>guruapi/Admin/proposal" class="nav-link text-success "><i data-feather="send"></i><span>Proposal</span></a>
            </li>
            <li class="dropdown">
              <a href="<?php echo base_url();?>guruapi/Admin/category" class="nav-link text-warning "><i data-feather="list"></i><span>All Category</span></a>
            </li>
              
            
           <!--  <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="mail"></i><span>Email</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="email-inbox.html">Inbox</a></li>
                <li><a class="nav-link" href="email-compose.html">Compose</a></li>
                <li><a class="nav-link" href="email-read.html">read</a></li>
              </ul>
            </li>  -->  
          </ul>
        </aside>
      </div>
      <!-- Main Content -->
      <div class="main-content">
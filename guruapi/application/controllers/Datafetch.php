<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datafetch extends CI_Controller {
	//////////////////////////////////////////////////////////////////////////////////////////////////

  function __construct() {
        parent::__construct();
        $this->load->model('Jobs_model');
        $this->load->model('Pagenumber_model');
        $this->load->model('Messages_model');
    }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////
	private function access_token(){
        // require_once APPPATH."third_party/OAuth2/Autoloader.php";
        
         $curl = curl_init();    
          curl_setopt_array($curl, array( 
          CURLOPT_URL => "https://www.guru.com/api/v1/oauth/token/access",    
          CURLOPT_RETURNTRANSFER => true,    
          CURLOPT_ENCODING => "",    
          CURLOPT_MAXREDIRS => 10,    
          CURLOPT_TIMEOUT => 30,    
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,   
          CURLOPT_CUSTOMREQUEST => "PUT",    
          CURLOPT_POSTFIELDS => "client_secret=7AD671EC-288D-443D-9B25-FE9BA6FAB804&grant_type=client_credentials&client_id=GCLI03095721T",  ));    
         $response = curl_exec($curl);  
         $err = curl_error($curl);    
         curl_close($curl);    
         if ($err) {    
          echo "cURL Error #:" . $err;  
        } 
        else 
          {   
            $json_result = json_decode(stripslashes($response), true);
           $json=str_replace("\\",'', $response);
           $jsondata=json_decode($json,true);
           //print_r($jsondata); 
           //echo $jsondata['access_token'];
           return $jsondata;
           }
   
    }
//  step A - single call with client ID and callback on the URL
private function getdata($authorize_url) {
  $response=$this->access_token();
  $access_token=$response['access_token'];
   //echo $access_token;
$curl = curl_init();
    $authorization = "Authorization: Bearer ".$access_token; // Prepare the authorisation token
    //echo $authorization;
  
    curl_setopt_array($curl, array(
        CURLOPT_URL => $authorize_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json' ,
            $authorization 
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {    
          echo "cURL Error #:" . $err;  
        } 
        else 
          {   
            $json_result = json_decode(stripslashes($response), true);
           $json=str_replace("\\",'', $response);
           $jsondata=json_decode($json,true);
           //print_r($jsondata); 
           //echo $jsondata['access_token'];
           return $jsondata;
           }
  }
 //////////////////////////////////////////////////////////////////////////////////////////////////////////
  private function jobs(){
  $i=1;
   while($i<=10){
    $jobsdata=$this->getdata($authorize_url="https://www.guru.com/api/v1/freelancer/jobs/messages/all/".$i."/50");
     //echo "<pre>";
   //print_r($jobsdata);die;
    $jobs=$jobsdata['Data'];
    foreach ($jobs as $job) {
       $jobid= $job['JobId'];
       $jobtitle=$job['JobTitle'];
       $lastmessage_date=date("Y-m-d H:i:s",$job['LastMessageDate']);
       $num_messages=$job['NumMessages'];

       echo $lastmessage_date;die;
       /////////////////////////////////////////////////////////////
    $jobsarray= array(
    'jobid' =>$jobid ,
    'jobtitle'=>$jobtitle,
    'lastmessage_date'=>$lastmessage_date,
    'num_message'=>$num_messages,
  );
  $pagenumbers=array(
    'jobid'=>$jobid,
    'pagenumber'=>$i,
  );
  $this->Jobs_model->create($jobsarray);
  $this->Pagenumber_model->create($pagenumbers); 
} 
$i++;
}
//die;
}
///////////////////////////////////////////////////////////////////////////////////////////////// 
 private function messages(){
 $jobs= $this->Jobs_model->read();
 foreach ($jobs as $job_id) { 
      $jobid=$job_id['jobid'];
  $i=1;
   while($i<=10){  
    //echo $i;
     $authorize_url="https://www.guru.com/api/v1/freelancer/jobs/".$jobid."/messages/all/".$i."/10";
     $messagesdata=$this->getdata($authorize_url);
     $data=$messagesdata['Data'];
     $messages=$data['Messages'];
      //print_r($messages);
    foreach ($messages as $message) {
       $senderid= $message['SenderId'];
       $sender_screen_name=$message['SenderScreenName'];
       $sendertype=$message['SenderType'];
       $dateposted=$message['DatePosted'];
       $message=$message['Message'];
       $is_viewed=@$message['IsViewed'];
      $filename=@$messages['0']['Attachments']['0']['FileName'];
        $fileurl=@$messages['0']['Attachments']['0']['FileURL'];
        $JobId=$data['JobId'];
      $jobtitle=$data['JobTitle'];
       $messagearray=array(
        'senderid'=>$senderid,
        'sender_screen_name'=>$sender_screen_name,
        'sender_type'=>$sendertype,
        'dateposted'=>$dateposted,
        'message'=>$message,
        'isviewed'=>$is_viewed,
        'filename'=>$filename,
        'fileurl'=>$fileurl,
        'jobid'=>$JobId,
        'JobTitle'=>$jobtitle,
      );
        $this->Messages_model->create($messagearray);
      } 
   $i++;    
 } 
}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

function send_data($authorize_url,$send_data)
{
  $response=$this->access_token();
  $access_token=$response['access_token'];
   // echo $access_token;
    $curl = curl_init();
    $authorization = "Authorization: Bearer ".$access_token; // Prepare the authorisation token
    // $data=json_decode($send_data,'true');
    curl_setopt_array($curl, array(
        CURLOPT_URL => $authorize_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('Data'=>array('Messages'=>$send_data)),
        // CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/json' ,
        //     $authorization 
        // ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {    
          echo "cURL Error #:" . $err;  
        } 
        else 
          {   
           return $response;
           }
}
/////////////////////////////////////////////////////////////////////////////////////////////////
 public function send_message()
 {
   $send_data='new 1';
      $authorize_url="https://www.guru.com/api/v1/freelancer/jobs/1656247/messages/send";
       $sendmessage=$this->send_data($authorize_url,$send_data);
    } 
/////////////////////////////////////////////////////////////////////////////////////////////////
}

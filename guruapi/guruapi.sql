-- SQL Schema for Guru API 

CREATE TABLE admin(
	admin_id INT(10) AUTO_INCREMENT PRIMARY KEY,
	admin_name VARCHAR(80) NOT NULL,
	admin_password VARCHAR(20) NOT NULL,
	admin_email VARCHAR(25) NOT NULL,
	admin_photo VARCHAR(30)
);
CREATE TABLE jobs(
	jobid_primary INT(10) AUTO_INCREMENT PRIMARY KEY,
	jobid INT(10) NOT NULL,
	jobtitle VARCHAR(10) NOT NULL,
	lastmessage_date VARCHAR(100) NOT NULL,
	num_message VARCHAR(100) NOT NULL,
	
);
CREATE TABLE messages(
	messageid INT(10) AUTO_INCREMENT PRIMARY KEY,
	senderid INT(100) NOT NULL,
	sender_screen_name VARCHAR(30)NOT NULL,
	sender_type VARCHAR(20)NOT NULL,
	dateposted VARCHAR(10) NOT NULL,
	message TEXT  NOT NULL,
	isviewed INT(10) NOT NULL,
	filename VARCHAR(100),
	fileurl Text,
	jobid INT(10) NOT NULL,
	jobtitle VARCHAR(20) NOT NULL
	
);
CREATE TABLE pagenumbers(
	pagenumber_id INT(10) AUTO_INCREMENT PRIMARY KEY,
	pagenumber INT(2) NOT NULL,
	jobid INT(10) NOT NULL
	
);
CREATE TABLE sendmessages(
	sendmessages_primary INT(10) AUTO_INCREMENT PRIMARY KEY,
	jobid INT(10) NOT NULL,
	jobtitle VARCHAR(20)NOT NULL,
	message VARCHAR(20)NOT NULL,
	sender_screen_name VARCHAR(10) NOT NULL,
	senderid TEXT  NOT NULL,
	sender_type CHAR(10) NOT NULL,
	dateposted VARCHAR(100) NOT NULL,
	filename VARCHAR(100),
	fileurl Text
);

CREATE TABLE category(
	categoryid_primary INT(10) AUTO_INCREMENT PRIMARY KEY,
	category_title VARCHAR(20) NOT NULL

);

CREATE TABLE category_message(
	category_message_id INT(10) AUTO_INCREMENT PRIMARY KEY,
	categoryid INT(10) NOT NULL,
	messageid INT(10) NOT NULL

);
-- Default data import
INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_password`, `admin_email`) 
VALUES (NULL, 'Admin', 'Admin@123', 'admin@guruapi.com');


INSERT INTO `category` (`categoryid`, `category_title`,`category_status`) 
VALUES (NULL, 'Category1',1);

INSERT INTO `category` (`categoryid`, `category_title`,`category_status`) 
VALUES (NULL, 'Category2',1);

INSERT INTO `category` (`categoryid`, `category_title`,`category_status`) 
VALUES (NULL, 'Category3',1);

INSERT INTO `category` (`categoryid`, `category_title`,`category_status`) 
VALUES (NULL, 'Category4',1);
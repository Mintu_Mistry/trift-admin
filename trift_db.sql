CREATE TABLE users (
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    user_name VARCHAR (200) NOT NULL,
    user_email VARCHAR (100) NOT NULL,
    user_contact VARCHAR (12) NOT NULL,
    user_address VARCHAR (100) NOT NULL,
    user_status INT(1) DEFAULT 0,
    user_profilepic VARCHAR (100) NOT NULL,
    date_added DATETIME DEFAULT NOW()

);

CREATE TABLE invites (
    invite_id INT(10) PRIMARY KEY AUTO_INCREMENT,
    invite_from INT(10) NOT NULL,
    invite_to INT(10) NOT NULL,
    invite_status INT(1) DEFAULT 1,
    date_added DATETIME DEFAULT NOW()
 
); 

CREATE TABLE gifts (
    gift_id INT(10) PRIMARY KEY AUTO_INCREMENT,
    gift_amount DECIMAL(8,2) NOT NULL,
    debtobligationid INT(10) NOT NULL,
    gift_mode VARCHAR (100) NOT NULL,
    gift_from INT(10) NOT NULL,
 	gift_to INT(10) NOT NULL,
 	cc_holdername VARCHAR(100) NOT NULL,
 	cc_number VARCHAR(20) NOT NULL,
 	cc_expdate VARCHAR(10) NOT NULL,
 	 date_added DATETIME DEFAULT NOW()
);

CREATE TABLE registries (
    registry_id INT(10) PRIMARY KEY AUTO_INCREMENT,
    userid INT(10) NOT NULL,
    registry_name VARCHAR(100) NOT NULL,
    registry_description VARCHAR(255) NOT NULL,
    registry_end_date DATETIME DEFAULT NOW(),
 	registry_code VARCHAR(50) NOT NULL,
 	registry_coverimage VARCHAR(100) NOT NULL,
    registry_status INT(1) DEFAULT 0,
 	 date_added DATETIME DEFAULT NOW()
);

CREATE TABLE debt_obligations (
    debtobligation_id INT(10) PRIMARY KEY AUTO_INCREMENT,
    debtobligation_title VARCHAR(100) NOT NULL,
    registryid INT(10) NOT NULL,
    goal_amount DECIMAL(8,2) NOT NULL,
    accountname VARCHAR(100) NOT NULL,
 	routingnumber VARCHAR(100) NOT NULL,
 	accountnumber VARCHAR(100) NOT NULL,
    debtobligation_status INT(1) DEFAULT 1,
 	 date_added DATETIME DEFAULT NOW()
);

CREATE TABLE messages (
    message_id INT(10) PRIMARY KEY AUTO_INCREMENT,
    message_from INT(10) NOT NULL,
    message_to INT(10) NOT NULL,
    message_text VARCHAR(255) NOT NULL,
 	 date_added DATETIME DEFAULT NOW()
);
CREATE TABLE admin (
    admin_id INT(10) PRIMARY KEY AUTO_INCREMENT,
    admin_name VARCHAR(100) NOT NULL,
    admin_email VARCHAR(100) NOT NULL,
    admin_password VARCHAR(25) NOT NULL
 	 
);
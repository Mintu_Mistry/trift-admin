<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Recent_search_model extends CI_Model {
public function __construct()
	{
		parent::__construct();
		$this->tablename = "recent_searches";  
	}

	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		return $this->db->insert_id();
	}

	function select($userid)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('userid',$userid);
		$this->db->group_by('search_text');
		$this->db->order_by('date_added', 'DESC');
		$this->db->limit(4);
		$query=$this->db->get();
		return $query->result_array();
	}
}
?>
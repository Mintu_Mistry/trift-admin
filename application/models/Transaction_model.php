<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "transactions";  
	}


	/* CRUD Operations for Message*/

	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return $this->db->insert_id();
	}

	function select($userid)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('userid',$userid);
		$query=$this->db->get();
		return $query->result_array();
	}

	function select_latest_transaction($userid)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('userid',$userid);
		$this->db->order_by('create_date, create_time','DESC');
		$this->db->limit(1);
		$query=$this->db->get();
		return $query->result_array();

	}

	function select_transaction_by_id($transaction_id)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('transaction_id',$transaction_id);
		$query=$this->db->get();
		return $query->result_array();
	}

}


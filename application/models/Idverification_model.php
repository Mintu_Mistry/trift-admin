<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Idverification_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "user_id_verification";
	}

	function curd_read($user_id ='')
	{	
		if($user_id =='')
		{
			$this->db->select('*');
			$this->db->from($this->tablename);
			$this->db->order_by('verification_id','ASC');
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from($this->tablename);
			$this->db->where('user_id',$user_id);
			$query=$this->db->get();
			return $query->result_array();
		}
		

	}
	
	function curd_create($data)
	{	
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return $this->db->insert_id();
	}
	
	function curd_update($data,$verification_id)
	{	
		$this->db->where("verification_id",$verification_id );
		$this->db->update($this->tablename,$data);
		return TRUE;

	}
	
}


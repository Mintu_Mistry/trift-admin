<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "users";  
	}
	/* CRUD Operations for users*/
	function checkuser($email){
		$data=$this->Users_model->select();
		//echo $this->db->last_query();die;
		if(@$data['user_email']==$email)
		 return true;
	}
	
	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return $this->db->insert_id();
	}

	function select()
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->order_by('user_id','DESC');
		$this->db->where('user_status!=2');
		$query=$this->db->get();
		return $query->result_array();

	}
	
	function update($data,$id)
	{
		$this->db->where('user_id',$id);
		$this->db->update($this->tablename,$data);
		return true;
	}

	function delete($id)
	{	
		 $this->db->query("UPDATE  debt_obligations SET debtobligations_status=2  WHERE registryid IN (SELECT registry_id FROM registries WHERE userid=$id)");
		 
		//echo $this->db->last_query();die;
		$this->db->where('userid',$id);
		$this->db->update('registries',array('registry_status'=>2));

		$this->db->where('user_id',$id);
		$this->db->update($this->tablename,array('user_status'=>2));
		//echo $this->db->last_query();die;
		return true;
	}
	function count()
	{
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('user_status!=2');
		$query=$this->db->get();
		return $query->num_rows();
	}
	function update_status($id,$status){
              if($status=='1'){
                  $status='0';
              }else{
                  $status='1';
              }
		 //$data=array('user_status'=>$status);
		 //print_r($data);
		$this->db->where('user_id',$id);		
		$this->db->update($this->tablename,array('user_status'=>$status));
		return true;
	}
	public function select_where($id,$showdeleted=false){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('user_id',$id);
		if(!$showdeleted){
			$this->db->where('user_status!=2');
		}
		
		$query=$this->db->get();
		return $query->result_array();

	}
	public function select_email($email,$showdeleted=false){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('user_email',$email);
		if(!$showdeleted){
			$this->db->where('user_status!=2');
		}
		$query=$this->db->get();
		return $query->result_array();

	}

	function CredentialsCheck($email,$password){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where("user_email", $email);
		$this->db->where("user_password", $password);
		$this->db->where("user_status!=2");
		//$check = $this->db->get($this->tablename)->row();
		$query=$this->db->get();
		return $query->row_array();
		/*if (isset($check->user_id)) {
			return $check->user_id;
		}
		else{
			return 0;
		}*/
	}

	public function generate_otp($email)
	{
		$otp = "";
		$length = 6;
        $numbers = "0123456789";
        for ($i = 0; $i < $length; $i++) {
            $otp .= $numbers[rand(0, strlen($numbers) - 1)];
        }
        $this->db->where("user_email",$email);
		$this->db->update($this->tablename, array("otp" => $otp));
		return $otp;
	}

	public function get_content_data(){
		$this->db->select('*');
		$this->db->from('content');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function user_social_id($id){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('social_id',$id);
		$this->db->where('user_status!=2');
		$query=$this->db->get();
		return $query->result_array();

	}

	function read_by_email($email)
	{
		$this->db->where('user_email',$email);
		$this->db->from($this->tablename);
		$query=$this->db->get();
		return $query->result_array();
	}

	function update_by_email($data,$email)
	{
		$this->db->where('user_email',$email);
		$this->db->update($this->tablename,$data);
		return true;
	}

	function read_by_user_name($name)
	{
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('user_status!=2');
		$this->db->like('user_name',$name);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function read_by_user_id($user_id,$showdeleted=false){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('user_id',$user_id);
		if(!$showdeleted){
			$this->db->where('user_status!=2');
		}
		
		$query=$this->db->get();
		return $query->row_array();

	}


	public function read_by_code($invite_code, $showdeleted=false){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('invite_code',$invite_code);
		if(!$showdeleted){
			$this->db->where('user_status!=2');
		}
		
		$query=$this->db->get();
		return $query->row_array();

	}
	
}


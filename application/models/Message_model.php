<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "messages";  
	}


	/* CRUD Operations for Message*/

	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return true;
	}

	function select_where($userid)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('message_from',$userid);
		$query=$this->db->get();
		return $query->result_array();

	}

	function update($data,$group_id)
	{
		
	}

	function delete($id)
	{	
		$this->db->where('user_id',$id);
		$this->db->delete($this->tablename);
	}

	
}


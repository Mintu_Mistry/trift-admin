<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "countries";  
		$this->tablename1 = "states";  
		$this->tablename2 = "cities";  
	}

	function select_country()
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->order_by('id','ASC');
		$query=$this->db->get();
		return $query->result_array();

	}
	
	function select_state($county_id)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename1);
		$this->db->where('country_id',$county_id);
		$this->db->order_by('id','ASC');
		$query=$this->db->get();
		return $query->result_array();

	}
	
	function select_city($state_id)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename2);
		$this->db->where('state_id',$state_id);
		$this->db->order_by('id','ASC');
		$query=$this->db->get();
		return $query->result_array();

	}
	
}


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "user_stripe_account";
	}
//stripe_account_id	user_id	account_holder_name	routing_number	account_number	stripe_bank_id	status	date_added
	function curd_read($user_id ='',$account_number ='')
	{	
	
		if($user_id !='')
		{
			if($account_number !='')
			{
				$this->db->select('*');
				$this->db->from($this->tablename);
				$this->db->where('user_id',$user_id);
				$this->db->where('account_number',$account_number);
				$this->db->order_by('stripe_account_id','ASC');
				$query=$this->db->get();
				return $query->result_array();
			}else{
				$this->db->select('*');
				$this->db->from($this->tablename);
				$this->db->where('user_id',$user_id);
				$query=$this->db->get();
				return $query->result_array();
			}
		}else{
			if($account_number !='')
			{
				$this->db->select('*');
				$this->db->from($this->tablename);
				$this->db->where('account_number',$account_number);
				$query=$this->db->get();
				return $query->result_array();
			}else{
				$this->db->select('*');
				$this->db->from($this->tablename);
				$this->db->order_by('stripe_account_id','ASC');
				$query=$this->db->get();
				return $query->result_array();
			}
		}
		

	}
	
	function curd_create($data)
	{	
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return $this->db->insert_id();

	}
	
}


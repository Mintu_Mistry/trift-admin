<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->tablename = "admin_detail";  
	}
	function select(){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$query=$this->db->get();
		return $query->row_array();

	}
	function checkcredentials($email,$password){
		$data=$this->Admin_model->select();
		if(@$data['admin_email']==$email && $data['admin_password']==$password)
		 return true;
	}
	function update($data)
	{
		
		$this->db->update('admin_detail',$data);
		return true;
	}
	function update_by_email($data, $email)
	{
		$this->db->where('admin_email', $email);
		$this->db->update('admin_detail',$data);
		return true;
	}
	 function select_admin_password($password)
	{
		$data=$this->Admin_model->select();
		if(@$data['admin_password']==$password)
		return true;
	}

}
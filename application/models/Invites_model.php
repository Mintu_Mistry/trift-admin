<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invites_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "invites";  
	}


	/* CRUD Operations for users*/

	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return true;
	}

	function select()
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$query=$this->db->get();
		return $query->result_array();

	}
	function select_where($userid)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('invite_from',$userid);
		$query=$this->db->get();
		return $query->result_array();

	}

	public function update_invite_referal($data, $invite_from, $invite_to)
	{	
		$this->db->where("invite_from",$invite_from);
		$this->db->where("invite_to",$invite_to);
		$this->db->update($this->tablename, $data);
		return true;
	}

	public function count_referal($userid)
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('invite_from',$userid);
		$this->db->where('invite_status', 3);
		$query=$this->db->get();
		return $query->result_array();

	}

	function delete($id)
	{	
		$this->db->where('user_id',$id);
		$this->db->delete($this->tablename);
	}

	
}


<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Registry_model extends CI_Model {
public function __construct()
	{
		parent::__construct();
		$this->tablename = "registries";  
	}


	/* CRUD Operations for Groups*/

	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		return $this->db->insert_id();
	}

	function select()
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->order_by('registry_id','DESC');
		$this->db->where('registry_status!=2');
		$query=$this->db->get();
		return $query->result_array();
	}
	public function select_where($id,$showdeleted=false){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('registry_id',$id);
		if(!$showdeleted){
			$this->db->where('registry_status!=2');
		}
		$query=$this->db->get();
		return $query->result_array();

	}
	public function is_expired($registry_id){
		@$rend_date= $this->Registry_model->select_where($registry_id)[0]['registry_end_date']; 
    	$registry_end_date=date("Y-m-d",strtotime($rend_date));
      	$current_date=date("Y-m-d");
      	// echo $current_date;
      	// echo "<pre>";
      	// echo $registry_end_date;
      	if($current_date <= $registry_end_date){
      		return true;
      	}
      	else{
      		return false;
      	}

	}
	function select_user_registry($userid){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('userid',$userid);
		$this->db->where('registry_status!=2');
		$this->db->order_by('registry_id','DESC');
		$query=$this->db->get();
		return $query->result_array();
	}
	
	function update($data,$group_id)
	{
		
	}

	function delete($id)
	{	
		//$status=array('status'=>2);
		//echo $id;
		 $this->db->where('registryid',$id);
		 $this->db->update('debt_obligations',array('debtobligations_status'=>2));
		 $this->db->where('registry_id',$id);
		 $this->db->update($this->tablename,array('registry_status'=>2));
		return true;
	}
	function count()
	{
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('registry_status!=2');
		$query=$this->db->get();
		return $query->num_rows();
	}
	function update_status($id,$status){
              if($status=='1'){
                  $status='0';
              }else{
                  $status='1';
              }
		 //$data=array('user_status'=>$status);
		 //print_r($data);
		$this->db->where('registry_id',$id);		
		$this->db->update($this->tablename,array('registry_status'=>$status));
		return true;
	}

	function crud_delete($registry_id)
	{	
		$this->db->where('registry_id', $registry_id);
		$this->db->delete('registries');
		return true;
	}

	function search_registry($search){
		$this->db->select('*');
		$this->db->from('registries');
		$this->db->where('registry_status!=2');
		$this->db->like('registry_name', $search);
		$this->db->or_like('registry_code', $search);
		return $this->db->get()->result_array();
	}
	
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debtobligation_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "debt_obligations";  
	}


	/* CRUD Operations for users*/

	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return $this->db->insert_id();
	}

	function select()
	{	
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->order_by('debtobligation_id','DESC');
		$this->db->where('debtobligations_status!=2');
		$query=$this->db->get();
		return $query->result_array();

	}
	public function select_where($id,$showdeleted=false){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('debtobligation_id',$id);
		if(!$showdeleted){
			$this->db->where('debtobligations_status!=2');
		}
		
		$query=$this->db->get();
		return $query->result_array();

	}
	function select_registry_debtobligation($registryid){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('registryid',$registryid);
		$this->db->where('debtobligations_status!=2');
		$this->db->where('debtobligations_status!=0');
		$this->db->order_by('registryid','DESC');
		$query=$this->db->get();
		return $query->result_array();
	}
	function update($data,$debtobligation_id)
	{
		$this->db->where('debtobligation_id', $debtobligation_id);
		$this->db->update('debt_obligations', $data);
		return true;
	}

	function delete($id)
	{	
		$this->db->where('debtobligation_id',$id);
		$this->db->update($this->tablename, array('debtobligations_status' =>2  ));
		return true;
	}

	function count()
	{
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('debtobligations_status!=2');
		$query=$this->db->get();
		return $query->num_rows();
	}
	function update_status($id,$status){
              if($status=='1'){
                  $status='0';
              }else{
                  $status='1';
              }
		 //$data=array('user_status'=>$status);
		 //print_r($data);
		$this->db->where('debtobligation_id',$id);		
		$this->db->update($this->tablename,array('debtobligations_status'=>$status));
		return true;
	}

	function select_dbt_name()
	{	
		$this->db->select('debtobligation_id, debtobligation_title');
		$this->db->from($this->tablename);
		$this->db->order_by('debtobligation_id','DESC');
		$this->db->where('debtobligations_status!=2');
		$query=$this->db->get();
		return $query->result_array();

	}

	function delete_debtobligation($id)
	{	
		$this->db->where('debtobligation_id',$id);
		$this->db->from($this->tablename);
		$this->db->delete();
	}
	
}


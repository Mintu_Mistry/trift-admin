<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gifts_model extends CI_Model {
	

	 public function __construct()
	{
		parent::__construct();
		$this->tablename = "gifts";  
	}


	/* CRUD Operations for users*/

	function create($data)
	{
		$this->db->insert($this->tablename,$data);
		//echo $this->db->last_query();
		return true;
	}

	function select()
	{	
		$this->db->select('*','users.user_name');
		$this->db->from($this->tablename);
		$this->db->order_by('gift_id','DESC');
		$query=$this->db->get();
		return $query->result_array();

	}
	function select_giftpayment($userid){
		$this->db->select('*','users.user_name');
		$this->db->from($this->tablename);
		$this->db->where('gift_from',$userid);
		$this->db->order_by('gift_id','DESC');
		$query=$this->db->get();
		return $query->result_array();
	}
	 function select_where($debtobligationid){
	 	$this->db->select('*','users.user_name');
		$this->db->from($this->tablename);
	 	$this->db->where('debtobligationid',$debtobligationid);
	 	$this->db->order_by('gift_id','DESC');
	 	$query=$this->db->get();
		return $query->result_array();
	 }

	function update($data,$group_id)
	{
		
	}

	function delete($id)
	{	
		$this->db->where('user_id',$id);
		$this->db->delete($this->tablename);
	}

	function count()
	{
		$this->db->select('*');
		$this->db->from($this->tablename);
		$query=$this->db->get();
		return $query->num_rows();
	}
	function select_debtobligation_gifts($debtobligation_id){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('debtobligationid',$debtobligation_id);
		$this->db->order_by('gift_id','DESC');
		$query=$this->db->get();
		return $query->result_array();
	}
	function get_progress_amt($debtobligation_id){
		$this->db->select('SUM(gift_amount) as progress' );
		$this->db->from($this->tablename);
		$this->db->where('debtobligationid',$debtobligation_id);
		$this->db->order_by('gift_id','DESC');
		$query=$this->db->get();
		return $query->result_array();
	}

	function select_gifter($user_id){
		$this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('gift_to',$user_id);
		$this->db->order_by('gift_id','DESC');
		$query=$this->db->get();
		return $query->result_array();
	}
}


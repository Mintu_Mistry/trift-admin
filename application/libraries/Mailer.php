<?php 

// Custom Mailer Library for CI using PHPMailer

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer
{
	private $mailer;

  public function __construct(){
      // log_message('Debug', 'PHPMailer class is loaded.');

  	require_once APPPATH.'third_party/PHPMailer/Exception.php';
      require_once APPPATH.'third_party/PHPMailer/PHPMailer.php';
      require_once APPPATH.'third_party/PHPMailer/SMTP.php';
      
      $this->mailer = new PHPMailer(true);  //load mailer with Exceptions support
  }

  public function sendMail($to,$subject,$message,$attachments = array()){

     	try {
     	 $this->mailer->setFrom('no-reply@trift.yesitlabs.xyz', 'Trift Team');
   		 $this->mailer->addAddress($to);
	       
       foreach($attachments as $file){
   			$this->mailer->addAttachment($file); 
       }

       $this->mailer->isHTML(true);

       $this->mailer->Subject = $subject;
	     $this->mailer->Body    = $message;

	     $this->mailer->send();
 		} 
 		catch (Exception $e) {
       	echo "Message could not be sent. Mailer Error: {$this->mailer->ErrorInfo}";
 		}
     	
  }
}
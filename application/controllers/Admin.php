<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->model('Debtobligation_model');
        $this->load->model('Gifts_model');
        $this->load->model('Invites_model');
        $this->load->model('Registry_model');
        $this->load->model('Users_model');
        $this->load->model('Transaction_model');
    }
//////////////////////////////////////////////////////////////////////////////////////////////////
    private function loadView($viewname,$data = array())
    {
        $admindata['admin']=$this->Admin_model->select();
        $this->load->view("header",$admindata);
        $this->load->view($viewname,$data);
        $this->load->view("footer");
    }
///////////////////////////////////////////////////////////////////////////////////////////////////
    private function checklogin(){
	//var_dump($this->session);die;
       if(!isset($this->session->email)){
          redirect('admin/login');
          die;
      }
  }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////dashboard////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public function index() 
  {
      $this->checklogin();
		// print_r($_SESSION);
      $count['users']=$this->Users_model->count();
      $count['registry']=$this->Registry_model->count();
      $count['debt_obligation']=$this->Debtobligation_model->count();
      $count['gifts']=$this->Gifts_model->count();
      $this->loadView('dashboard',$count);
  }
//////////////////////////////////////////////////////////////////////////////////////
  public function login(){

      if(isset($_POST['submit'])){
         $email=$this->input->post('email');
         $password=$this->input->post('password');
         $admindata['admin']=$this->Admin_model->select();
         if($this->Admin_model->checkcredentials($email,$password)){
           $this->session->email=$email;
            if(!empty($this->input->post('rememberme'))){  
                setcookie ("login_email",$this->input->post('email'),time()+ (10 * 365 * 24 * 60 * 60)); 
                setcookie ("login_password", $this->input->post('password'),time()+ (10 * 365 * 24 * 60 * 60));
            }  
            else{  
                if(isset($_COOKIE["login_email"])){  
                 setcookie("login_email","");  
                }  
                if(isset($_COOKIE["login_password"])){  
                 setcookie("login_password","");  
                }  
            }
           redirect('admin');
       }
       else{
        $this->session->set_flashdata('error','Invalid Email or Password!');
    } 	
}
$admindata['admin']=$this->Admin_model->select();
$this->load->view('login',$admindata);
}
public function forget_password_email()
{
    $emailid = $this->input->post('emailid');
    $admindata = $this->Admin_model->select();
    if($admindata['admin_email'] != $emailid){
        echo 2;
    }
    else{
        $otp = "";
        $length = 4;
        $numbers = "0123456789";
        for ($i = 0; $i < $length; $i++) {
            $otp .= $numbers[rand(0, strlen($numbers) - 1)];
        }
        $subject = 'Forget Password';
        $message = 'Your OTP is '.$otp.'';
        $this->session->otp = $otp;

        if(mail($emailid, $subject, $message)) {
            echo 1;
        }
        else {
            echo 0;    
        } 
    }
}

public function verify_otp($user_otp)
{
    if ($user_otp == $_SESSION['otp']) {
        echo "1";
    } else {
        echo "0";
    }
}

public function new_password()
{   
    $email = $this->input->post('email');
    $new_pass = $this->input->post('new_pass');
    $confirm_pass = $this->input->post('confirm_pass');
    $admindata = $this->Admin_model->select();
    if($admindata['admin_password'] != $new_pass) {
      if($new_pass == $confirm_pass) {
          $this->Admin_model->update_by_email(array("admin_password" => $new_pass), $email);
          
          echo "1";
      }
      else{
          echo "2";
      }
    }
    else{
        echo "0";
    }
}

public function forget_pass(){
    $this->load->view("forgetpass");
}
//////////////////////////////////////////////////////////////////////////////////////   
public function logout()
{
  session_destroy();
  redirect('admin/login');

}
//////////////////////////////////////////////////////////////////////////////////  
public function profile(){
    $data['profile']=$this->Admin_model->select();
        //print_r($data);
    $this->loadView('profile',$data);
}
/////////////////////////////////////////////////////////////////////////////////    
public function edit_profilesubmit(){
    $password=$this->input->post('password');
    $npassword=$this->input->post('npassword');
    $cpassword=$this->input->post('cpassword');
    if(!empty($password)&& empty($npassword) && empty($cpassword)){
        $this->session->set_flashdata('error', 'New password and Confirm Password should not be blank ');
        redirect('admin/profile');
    }
    if(!empty($password)&& !empty($npassword) && empty($cpassword)){
        $this->session->set_flashdata('error', 'Confirm Password should not be blank ');
        redirect('admin/profile');
    }
    if(!empty($password) && !empty($npassword) && !empty($cpassword)){    
        if($this->Admin_model->select_admin_password($password)){
          if($password==$npassword){
            $this->session->set_flashdata('error', 'Old password and New Password should not be same ');
            redirect('admin/profile');
        }
        else{
           if($npassword == $cpassword){

            $tmp_name=$_FILES['adminprofile']['tmp_name'];
            $file_name=$_FILES['adminprofile']['name'];
            if (empty($file_name)){
                $data = array(
                    'admin_name' =>$this->input->post('admin_name') ,
                    'admin_email' =>$this->input->post('admin_email') ,
                    'admin_password'=>$cpassword,
                ); 
            }
            else{
                $data = array(
                    'admin_name' =>$this->input->post('admin_name') ,
                    'admin_email' =>$this->input->post('admin_email') ,
                    'admin_password'=>$cpassword,
                    'admin_profilepic'=>$file_name,
                );
                move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
            }
            if($this->Admin_model->update($data))
            {
                $this->session->set_flashdata('success', 'Password Updated Successfully');
            }
            else{
                $this->session->set_flashdata('error', 'Something went wrong ');
            }
        } 

        else{
            $this->session->set_flashdata('error', 'New password and Confirm Password should be same ');
            redirect('admin/profile');
        }
    }
}
else{
   $this->session->set_flashdata('error', 'Old Password Wrong');
            // print_r($_SESSION);
            // die;
   redirect('admin/profile');
}
}

else{
   $tmp_name=$_FILES['adminprofile']['tmp_name'];
   $file_name=$_FILES['adminprofile']['name'];
   if (empty($file_name)){
    $data = array(
        'admin_name' =>$this->input->post('admin_name') ,
        'admin_email' =>$this->input->post('admin_email') ,  
    ); 
}
else{
    $data = array(
        'admin_name' =>$this->input->post('admin_name') ,
        'admin_email' =>$this->input->post('admin_email') ,
        'admin_profilepic'=>$file_name,
    );
    move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
}
if($this->Admin_model->update($data))
{
    $this->session->set_flashdata('success', ' Password Updated Successfully');
}
else{
    $this->session->set_flashdata('error', 'Something went wrong ');
}
}
redirect('admin');


}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////user////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public function users(){
   $data['users']=$this->Users_model->select();
   $this->loadView('users',$data);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
public function add_user(){

   $this->loadView('add_user');
}
/////////////////////////////////////////////////////////////////////////////////////////////////    
public function add_usersubmit(){
    $tmp_name=$_FILES['userprofile']['tmp_name'];
    $file_name=$_FILES['userprofile']['name'];
    $default_image='images.png';
    $email=$this->input->post('useremail');
        //print_r($email);
         //$this->Users_model->checkuser($email);
         //echo $this->db->last_query();die;
    if($this->Users_model->select_email($email))
    {        
        $this->session->set_flashdata('error2', 'User Already Exists ');
    }
    else
    {                    
        if(!empty($file_name)){
            $data = array(
               'user_name' =>$this->input->post('username') ,
               'user_password' =>$this->input->post('password') ,
               'user_email' =>$this->input->post('useremail') ,
               'user_status'=>$this->input->post('status'),
               //'user_contact'=>$this->input->post('contact'),
               'user_address'=>$this->input->post('address'),
               'user_profilepic'=>$file_name,
           ); 
        }else{
            $data = array(
                'user_name' =>$this->input->post('username') ,
                'user_password' =>$this->input->post('password') ,
                'user_email' =>$this->input->post('useremail') ,
                'user_status'=>$this->input->post('status'),
                //'user_contact'=>$this->input->post('contact'),
                'user_address'=>$this->input->post('address'),
                'user_profilepic'=>$default_image,
            ); }
            move_uploaded_file($tmp_name, 'assets/images/'.$file_name);


        //print_r(@$data);
            if($this->Users_model->create($data))
            {
                $this->session->set_flashdata('sucess', 'User Added Successfully');
            }
            else{
                $this->session->set_flashdata('error2', 'Something went wrong ');
            }
        }
       // echo $this->db->last_query();
        redirect('admin/users');

    }
/////////////////////////////////////////////////////////////////////////////////////////////////    
    public function edit_user($id){	
    	$data['user']=$this->Users_model->select_where($id)[0];
    	$this->loadView('edit_user',$data);
    }
 /////////////////////////////////////////////////////////////////////////////////////////////////   
    public function edit_usersubmit(){
        $id=$this->input->post('userid');
        $tmp_name=$_FILES['userprofile']['tmp_name'];
        $file_name=$_FILES['userprofile']['name'];
        if (empty($file_name)){

            $data = array(
                'user_name' =>$this->input->post('username') ,
                'user_password' =>$this->input->post('password') ,
                'user_email' =>$this->input->post('useremail') ,
                'user_status'=>$this->input->post('status'),
                //'user_contact'=>$this->input->post('contact'),
                'user_address'=>$this->input->post('address'),

            ); }
            else{
                $data = array(
                   'user_name' =>$this->input->post('username') ,
                   'user_password' =>$this->input->post('password') ,
                   'user_email' =>$this->input->post('useremail') ,
                   'user_status'=>$this->input->post('status'),
                   //'user_contact'=>$this->input->post('contact'),
                   'user_address'=>$this->input->post('address'),
                   'user_profilepic'=>$file_name,
               );
        //print_r($data);
                move_uploaded_file($tmp_name, 'assets/images/'.$file_name);}
                if($this->Users_model->update($data,$id))
                {
                    $this->session->set_flashdata('success', ' User Updated Successfully');
                }
                else{
                    $this->session->set_flashdata('error', 'Something went wrong ');
                }
                redirect('admin/users');
            }
 /////////////////////////////////////////////////////////////////////////////////////////////////   
            public function user_status($id,$status){
        //echo $id;
        //echo $status;
               if($this->Users_model->update_status($id,$status))
               {
                $this->session->set_flashdata('success', ' User Status Updated Successfully');
            }
            else{
                $this->session->set_flashdata('error', 'Something went wrong ');
            }
            redirect('admin/users');
        }
//////////////////////////////////////////////////////////////////////////////////////////////////
        public function user_delete($id){	 
           if($this->Users_model->delete($id))
           {
            $this->session->set_flashdata('success', ' User Deleted Successfully');
        }
        else{
            $this->session->set_flashdata('error', 'Something went wrong ');
        }
        redirect('admin/users');
    }
    

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////registry///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function registry($userid=0){
        //echo $userid;
        if($userid!=0){
            $data['userid']=$userid;
            $data['registry']=$this->Registry_model->select_user_registry($userid);
        }
        else{
           $data['registry']=$this->Registry_model->select();
       }
       $this->loadView('registry',$data);
   }
 //////////////////////////////////////////////////////////////////////////////////////////////   
   public function registry_delete($id){

       if($this->Registry_model->delete($id))
       {
        $this->session->set_flashdata('success', 'Registry Deleted Successfully');
    }
    else{
        $this->session->set_flashdata('error', 'Something went wrong ');
    }
    redirect('admin/registry');
}
//////////////////////////////////////////////////////////////////////////////////////////////
public function registry_status($id,$status){
        //echo $id;
        //echo $status;
    if($this->Registry_model->update_status($id,$status))
    {
        $this->session->set_flashdata('success', ' Registry Status updated Successfully');
    }
    else{
        $this->session->set_flashdata('error', 'Something went wrong ');
    }
    $userid=intval($this->input->get('userid'));
        //echo $userid; die;
    if($userid>0){
        redirect('admin/registry/'.$userid);
    }
    else{
       redirect('admin/registry');
   }   
}



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////debt_obligations/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public function debt_obligation($registryid=0){
    if($registryid!=0){
        $data['registryid']=$registryid;
        $data['debt_obligation']=$this->Debtobligation_model->select_registry_debtobligation($registryid);  
    }
    else{
    	$data['debt_obligation']=$this->Debtobligation_model->select();
    }
    $this->loadView('debt_obligation',$data);
}
///////////////////////////////////////////////////////////////////////////////////////////////    
public function debtobligation_delete($id){

   if($this->Debtobligation_model->delete($id))
   {
    $this->session->set_flashdata('success', ' Debt Obligation Deleted Successfully');
}
else{
    $this->session->set_flashdata('error', 'Something went wrong ');
} 
redirect('admin/debt_obligation');
}
/////////////////////////////////////////////////////////////////////////////////////////////////    
public function debtobligation_status($id,$status){
        //echo $id;
        //echo $status;
    if($this->Debtobligation_model->update_status($id,$status))
    {
        $this->session->set_flashdata('success', ' Debt Obligation Status Updated Successfully');
    }
    else{
        $this->session->set_flashdata('error', 'Something went wrong ');
    } 
    $registryid=intval($this->input->get('registryid'));
        //echo $registryid; die;
    if($registryid>0){
        redirect('admin/debt_obligation/'.$registryid);
    }
    else{
       redirect('admin/debt_obligation');
   }   
}

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////gifts////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public function gifts($debtobligation_id=0){
    if($debtobligation_id!=0)
    {
           // echo $debtobligation_id;
        $data['debtobligation_id']=$debtobligation_id;
        $data['gifts']=$this->Gifts_model->select_debtobligation_gifts($debtobligation_id);
        
    }

    else{
        $data['gifts']=$this->Gifts_model->select();
    }
    $this->loadView('gifts',$data);
    
}
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////invites/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public function invites(){
   $data['invites']=$this->Invites_model->select();
   $this->loadView('invites',$data);
}

  public function transactions($userid=0){
    $data['userid']=$userid;
    $data['transaction_details']=$this->Transaction_model->select($userid);
    $this->loadView('transactions',$data);
  }
}

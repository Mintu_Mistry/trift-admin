<?php
defined('BASEPATH') OR exit('No direct script access allowed');

error_reporting(1);

class Apis extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->model('Debtobligation_model');
        $this->load->model('Gifts_model');
        $this->load->model('Invites_model');
        $this->load->model('Registry_model');
        $this->load->model('Users_model');
        $this->load->model('Message_model');
        $this->load->model('Transaction_model');
        $this->load->model('Recent_search_model');
    }

    /*
        Purpose   : Login User   
        method    : POST[form-data]
        Parameter : { user_email*,user_password*}=MANUAL
        URL       : https://trift.yesitlabs.xyz/apis/login_user
        Response  : JSON
    */ 
    public function login_user(){
        $userData = array();
        $user_email = $this->input->post('user_email');
        $user_password = $this->input->post('user_password');
        $fcm_token = $this->input->post('fcm_token');
        if(!empty($user_email) && !empty($user_password)){
            $user_details = $this->Users_model->CredentialsCheck($user_email,$user_password);
            if(!empty($user_details)){
                if($user_details['user_status'] == 0){
                    $response['status'] = false;
                    $response['message'] = 'User is inactive.' ; 
                }
                else {
                    $data['user_id'] = $user_details['user_id'];
                    $data['user_name'] = $user_details['user_name'];
                    $data['fcm_token'] = $user_details['fcm_token'];
                    $this->Users_model->update(["fcm_token" => $this->input->post("fcm_token")],$user_id);
                    $response['status'] = true;
                    $response['data'] = $data;
                    $response['message'] = 'User logged in successfully.' ; 
                }
                        
            }
            else{
                $response['status'] = false; 
                $response['message'] = 'Invalid User Email or Password.' ;
            }   
        }
        else {
            $response['status'] = false; 
            $response['message'] = 'Please provide required fields.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Social Login
        method    : POST [form-data]
        Parameter : {social_id*, fcm_token*, user_name*, user_email*}
        URL       : https://trift.yesitlabs.xyz/apis/social_login
        Response  : JSON
    */
    public function social_login() {
        $social_id = $this->input->post('social_id');
        $user_name = $this->input->post('user_name');
        $user_email = $this->input->post('user_email');
        $fcm_token = $this->input->post('fcm_token');
        $tmp_name = $_FILES['user_profilepic']['tmp_name'];
        $file_name = $_FILES['user_profilepic']['name'];
        $default_image = 'images.png';
        
        $user_status = 1;
        $date_added = date('Y-m-d h:i:s');
        //print_r($_POST);
        if (!empty($user_email) && !empty($user_name) && !empty($social_id) && !empty($fcm_token)) {
            $exist = $this->Users_model->user_social_id($social_id);
            if(!empty($exist)){
                if($exist[0]['user_status'] == 0){
                    $response['status'] = false; 
                    $response['message'] = 'Inactive user.' ;
                }
                else {
                    $exist_data['user_id'] = $exist[0]['user_id'];
                    $exist_data['user_name'] = $exist[0]['user_name'];
                    $exist_data['fcm_token'] = $exist[0]['fcm_token'];
                    $response['status'] = true; 
                    $response['data'] = $exist_data;
                    $response['message'] = 'Logged in successfully.' ;
                }
            }
            else {
                if (empty($file_name)){
                    $insert_id = $this->Users_model->create(array(
                        'user_name' => $user_name,
                        'user_email' => $user_email,
                        'fcm_token' => $fcm_token,
                        'social_id' => $social_id,
                        'date_added' => $date_added,
                        'user_profilepic' => $default_image,
                        'user_status' => $user_status
                    ));
                }
                else{
                    $insert_id = $this->Users_model->create(array(
                        'user_name' => $user_name,
                        'user_email' => $user_email,
                        'fcm_token' => $fcm_token,
                        'social_id' => $social_id,
                        'date_added' => $date_added,
                        'user_profilepic' => $file_name,
                        'user_status' => $user_status
                    ));
                    move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
                }
                $user_data = $this->Users_model->select_where($insert_id);
                if($insert_id){
                    $data['user_id'] = "$insert_id";
                    $data['user_name'] = $user_data[0]['user_name'];
                    $data['fcm_token'] = $fcm_token;
                    $response['status'] = true; 
                    $response['data'] = $data;
                    $response['message'] = 'Registered successfully.' ;
                } else {
                    $response['status'] = false; 
                    $response['message'] = 'Some goes wrong, please try again!.' ;
                }
            }
        } else {
            $response['status'] = false; 
            $response['message'] = 'Please provide required fields.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Forgot Password
        method    : POST [form-data]
        Parameter : { user_email* }
        URL       : https://trift.yesitlabs.xyz/apis/forget_password
        Response  : JSON
    */
    public function forget_password() {
        $user_email = $this->input->post("user_email");
        if(!empty($user_email)){
            $data = $this->Users_model->select_email($user_email);
            if(!empty($data)){   
                $otp = $this->Users_model->generate_otp($user_email);
                $message = 'Your Verification OTP Is :-'.$otp;
                $this->load->library('email');
                $this->email->set_newline("\r\n");
                $this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
                $this->email->to($user_email);
                $this->email->subject('Forget Password');
                $this->email->message($message);
                $send_status = $this->email->send();
                if ($send_status) {
                    $response['status'] = true;  
                    $response['otp'] = $otp;                  
                    $response['message'] = 'OTP is sent on provided email!' ;
                } else {                               
                    $response['status'] = false;                    
                    $response['message'] = 'Sorry, there is some internal issue, please try again';
                }
            }
            else {
                $response['status'] = false; 
                $response['message'] = 'User does not exist.' ;
            }
        }
        else {
            $response['status'] = false; 
            $response['message'] = 'Please provide required fields.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : change password .
        method    : POST[form-data]
        Parameter : {user_id*,npassword*,cpassword*}     
        URL       : https://trift.yesitlabs.xyz/apis/change_password
        Response  : JSON 
    */
    public function change_password(){
        $user_email = $this->input->post('user_email');
        $password = $this->input->post('password');
        if(!empty($user_email) && !empty($password)){
            $user_data = $this->Users_model->read_by_email($user_email);
            if(!empty($user_data)){
                $data = array(
                    'user_password' => $password,
                );
                if($this->Users_model->update_by_email($data,$user_email))
                {
                   $response['status'] = true;                   
                   $response['message'] = 'Password Updated Successfully.';
                }
                else{
                    $response['status'] = false;                   
                    $response['message'] = 'Something went wrong.';
                }
            }
            else {
                $response['status'] = false;                   
                $response['message'] = 'User doest not exist.';
            }
        }
        else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Check Email  
        method    : POST[form-data]
        Parameter : { email*}
        URL       : https://trift.yesitlabs.xyz/apis/check_email
        Response  : JSON
    */
    public function check_email()
    {
        $returndata = array();
        $email = $this->input->post("email");
        if(!empty($email)){
            $user_data = $this->Users_model->select_email($email);
            if(!empty($user_data)){
                $returndata['status'] = true;
                $returndata['message'] = "Email already registered!";
            }
            else{
                $returndata['status'] = false;
                $returndata['message'] = "Email not registered!";
            }
        }
        else {
            $returndata['status'] = false;
            $returndata['message'] = "Please provide required fields.";
        }
        header('Content-Type: application/json');
        echo json_encode($returndata);
    }

    /*
        Purpose   : Register user  
        method    : POST[form-data]
        Parameter : { user_email*,user_password*,user_address*,user_name*,user_contact*,user_profilepic* }=MANUAL
        URL       : https://trift.yesitlabs.xyz/apis/register_user
        Response  : JSON
    */ 
    public function register_user(){    
        //$userData = array();
        /*$userData['user_name'] = $this->input->post('user_name');
        $userData['user_email'] = $this->input->post('user_email');
        $userData['user_password'] = $this->input->post('user_password');
        $userData['date_added'] = date('Y-m-d h:i:s');
        $userData['user_status'] = 1;
        $userData['invite_code'] = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,10);
        $userData['fcm_token'] = $this->input->post('fcm_token');
        $tmp_name = $_FILES['user_profilepic']['tmp_name'];
        $userData['user_profilepic'] = $_FILES['user_profilepic']['name'];
        $default_image = 'images.png';*/

        $user_name = $this->input->post('user_name');
        $user_email = $this->input->post('user_email');
        $user_password = $this->input->post('user_password');
        $date_added = date('Y-m-d h:i:s');
        $user_status = 1;
        $invite_code = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,10);
        $fcm_token = $this->input->post('fcm_token');
        $tmp_name = $_FILES['user_profilepic']['tmp_name'];
        $file_name = $_FILES['user_profilepic']['name'];
        $default_image = 'images.png';

        if (!empty($user_email) && !empty($user_name) && !empty($user_password) && !empty($fcm_token)) {
            $user_data = $this->Users_model->select_email($user_email);
            if (!empty($user_data))
            {
                $response['status'] = false;                          
                $response['message'] = 'User already exist or enter a valid email address.' ;
            }
            else
            {    
                if (empty($file_name)){
                    $insert_id = $this->Users_model->create(array(
                        'user_name' => $user_name,
                        'user_email' => $user_email,
                        'user_password' => $user_password,
                        'fcm_token' => $fcm_token,
                        'social_id' => $social_id,
                        'date_added' => $date_added,
                        'user_profilepic' => $default_image,
                        'invite_code' => $invite_code,
                        'user_status' => $user_status
                    ));
                }
                else{
                    $insert_id = $this->Users_model->create(array(
                        'user_name' => $user_name,
                        'user_email' => $user_email,
                        'user_password' => $user_password,
                        'fcm_token' => $fcm_token,
                        'social_id' => $social_id,
                        'date_added' => $date_added,
                        'user_profilepic' => $file_name,
                        'invite_code' => $invite_code,
                        'user_status' => $user_status
                    ));
                    move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
                }
                $response['status'] = true;       
                $response['user_id'] = $insert_id;                    
                $response['message'] = 'User registered successfully.' ;
            }
        }
        else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get Home Data
        method    : Post[form-data]
        Parameter : {user_id*}     
        URL       :https://trift.yesitlabs.xyz/apis/get_home_data
        Response  : JSON 
    */
    public function get_home_data(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $user_data = $this->Users_model->select_where($user_id);
            //print_r($user_data);
            
            $user_reg_data = $this->Registry_model->select_user_registry($user_id);
            if ($user_data || $user_reg_data) {
                $reg_details = array();
                foreach ($user_reg_data as $user_reg) {
                    $dbt_details = $this->Debtobligation_model->select_registry_debtobligation($user_reg['registry_id']);
                    $dbt_obligation = array();
                    $archieve_amount = 0;
                    foreach ($dbt_details as $dbt) {
                        $dbt_obligation[] = array(
                            "debtobligation_id" => $dbt['debtobligation_id'],
                            "registry_id" => $dbt['registryid'],
                            "goal_amount" => $dbt['goal_amount']
                        );
                        $archieve_amount += $dbt['goal_amount'];
                    }

                    $reg_details[] = array(
                        "registry_id" => $user_reg['registry_id'],
                        "userid" => $user_reg['userid'],
                        "registry_name" => $user_reg['registry_name'],
                        "registry_coverimage" => $user_reg['registry_coverimage'],
                        "dbt_obligation" => $dbt_obligation,
                        "archieve_amount" => $archieve_amount
                    );
                }
                if(!empty($reg_details)){
                    $response['status'] = true;  
                    $response['data'] = $reg_details;
                    $response['message'] = 'Home data fetched successfully.';
                }
                else {
                    $response['status'] = false;                   
                    $response['message'] = 'No Data found.';
                }
            } else{
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get User details
        method    : Post[form-data]
        Parameter : {user_id*}     
        URL       :https://trift.yesitlabs.xyz/apis/get_user_details
        Response  : JSON 
    */
    public function get_user_details(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $data = $this->Users_model->select_where($user_id);
            if($data) {
                $user_data['user_id'] = $data[0]['user_id'];
                $user_data['user_name'] = $data[0]['user_name'];
                $user_data['user_email'] = $data[0]['user_email'];
                $user_data['user_contact'] = $data[0]['user_contact'];
                $user_data['user_address'] = $data[0]['user_address'];
                $user_data['user_status'] = $data[0]['user_status'];
                $user_data['user_profilepic'] = $data[0]['user_profilepic'];
                $user_data['date_added'] = $data[0]['date_added'];
                
                $response['status'] = true;  
                $response['data'] = $user_data;                                     
                $response['message'] = 'User details fetched successfully.';
            } else{
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Update user profile 
        method    : POST[form-data]
        Parameter : { user_id*,user_email*,user_password*,user_address*,user_name*,
        user_contact*,user_profilepic* }=MANUAL
        URL       : https://trift.yesitlabs.xyz/apis/update_user_profile
        Response  : JSON
    */ 
    public function update_user_profile(){    
        $tmp_name = $_FILES['user_profilepic']['tmp_name'];
        $file_name = $_FILES['user_profilepic']['name'];
        $default_image = 'images.png';
        if (empty($file_name)){
            $userData = array();
            $userid = $this->input->post('user_id');
            $userData['user_name'] = $this->input->post('user_name');
            $userData['user_address'] = $this->input->post('user_address');
            $userData['user_contact'] = $this->input->post('user_contact');
            $userData['user_status'] = 1;
            $userData['user_profilepic'] = $default_image; 
        }
        else{
            $userData = array();
            $userid = $this->input->post('user_id');
            $userData['user_name'] = $this->input->post('user_name');
            $userData['user_address'] = $this->input->post('user_address');
            $userData['user_contact'] = $this->input->post('user_contact');
            $userData['user_status'] = 1;
            $userData['user_profilepic'] = $file_name;
            
        }
        move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
        if(!empty($userid)){
            $insert = $this->Users_model->update($userData,$userid);
            //echo $this->db->last_query();die;
            if($insert){                    
                $response['status'] = true;                     
                $response['message'] = 'User Updated successfully.' ;
            }
            else{
                $response['status'] = false;                   
                $response['message'] = 'Some problems occurred, please try again.' ;
            }
        }
        else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get User Registries
        method    : Post[form-data]
        Parameter : {user_id*}     
        URL       :https://trift.yesitlabs.xyz/apis/get_user_registry
        Response  : JSON 
    */
    public function get_user_registry(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $data = $this->Registry_model->select_user_registry($user_id);
            if($data) {
                $response['status'] = true;  
                $response['data'] = $data;                                     
                $response['message'] = 'Registry details fetched successfully.';
            }else{
                $response['status'] = false;                   
                $response['message'] = 'No Data Found.';
            }
        } else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*********************
        Purpose   : Get registry by registry id .
        method    : Post[form-data]
        Parameter : {registry_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/get_registry_by_id
        Response  : JSON 
    */
    public function get_registry_by_id(){
        $registry_id = $this->input->post('registry_id');
        $today_date = date('m-d-Y');
        if (!empty($registry_id)){
            $user_reg_data = $this->Registry_model->select_where($registry_id);
            //print_r($user_reg_data);
            if (!empty($user_reg_data) ){
                $reg_details = array();
                foreach ($user_reg_data as $user_reg) {
                    if($today_date > date('m-d-Y', strtotime($user_reg['registry_end_date'])))
                    {
                        $response['status'] = false;                   
                        $response['message'] = 'No Data found.';
                    }    
                    else {
                        $dbt_details = $this->Debtobligation_model->select_registry_debtobligation($user_reg['registry_id']);
                        $dbt_obligation = array();
                        foreach ($dbt_details as $dbt) {
                            $gifters_data = $this->Gifts_model->select_debtobligation_gifts($dbt['debtobligation_id']);
                            $progress_amount = 0;
                            foreach ($gifters_data as $amt) {
                                $progress_amount += $amt['gift_amount']/100;
                            }
                            $goal_amt = intval($dbt['goal_amount']);
                             //$result = sum($p_amt_array);
                            $dbt_obligation[] = array(
                                "debtobligation_id" => $dbt['debtobligation_id'],
                                "debtobligation_title" => $dbt['debtobligation_title'],
                                "registry_id" => $dbt['registryid'],
                                "goal_amount" => "$goal_amt",
                                "gifters" => count($gifters_data),
                                "progress_amount" => $progress_amount
                            );
                        }
                        $reg_details[] = array(
                            "registry_id" => $user_reg['registry_id'],
                            "userid" => $user_reg['userid'],
                            "registry_name" => $user_reg['registry_name'],
                            "registry_description" => $user_reg['registry_description'],
                            "registry_code" => $user_reg['registry_code'],
                            "registry_end_date" => date('m-d-Y', strtotime($user_reg['registry_end_date'])),
                            "registry_coverimage" => $user_reg['registry_coverimage'],
                            "dbt_obligation" => $dbt_obligation
                        );
                        $response['status'] = true;  
                        $response['data'] = $reg_details;
                        $response['message'] = 'Registry details fetched successfully.';
                    }
                }
                
            } else {
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        }else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*********************
        Purpose   : Get All Registry.
        method    : Post[form-data]
        Parameter : {registry_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/get_all_registry
        Response  : JSON 
    */
    public function get_all_registry(){
        $data = $this->Registry_model->select();
        if ($data) {
            $response['status'] = true;  
            $response['data'] = $data;                                     
            $response['message'] = 'All Registry details fetched successfully.';
        } else{
            $response['status'] = false;                   
            $response['message'] = 'No Data Found.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get User Transactions
        method    : Post[form-data]
        Parameter : {user_id*}     
        URL       :https://trift.yesitlabs.xyz/apis/get_user_transactions
        Response  : JSON 
    */
    public function get_user_transactions(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $data = $this->Transaction_model->select($user_id);
            $transaction_data = array();
            foreach ($data as $trans_data) {
                $user_details = $this->Users_model->read_by_user_id($trans_data['receiver_id']);
                $transaction_data[] = array(
                    'transaction_id' => $trans_data['transaction_id'],
                    'userid' => $trans_data['userid'],
                    'receiver_id' => $trans_data['receiver_id'],
                    'receiver_name' => $trans_data['receiver_name'],
                    'receiver_profile_pic' => $user_details['user_profilepic'],
                    'amount' => ($trans_data['amount']/100),
                    'create_date' => $trans_data['create_date'],
                    'create_time' => date('h:i A', strtotime($trans_data['create_time'])),
                    'token_id' => $trans_data['token_id'],
                );
            }
            if($data) {
                $response['status'] = true;  
                $response['data'] = $transaction_data; 
                $response['message'] = 'Transaction details fetched successfully.';
            }else{
                $response['status'] = false;                   
                $response['message'] = 'No Data Found.';
            }
        } else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Registry Delete
        method    : Post[form-data]
        Parameter : {registry_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/delete_registry
        Response  : JSON 
    */
    public function delete_registry(){
        $registry_id = $this->input->post('registry_id');
        if (!empty($registry_id)){
            $data = $this->Registry_model->crud_delete($registry_id);
            if ($data) {
                $response['status'] = true;  
                $response['data'] = $data;                                     
                $response['message'] = ' Registry deleted successfully.';
            }else{
                $response['status'] = false;                   
                $response['message'] = 'Some problems occurred, please try again.';
            }
        }else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Gifters List
        method    : Post[form-data]
        Parameter : {user_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/get_gifters_data
        Response  : JSON 
    */
    public function get_gifters_data(){
        $debtobligation_id = $this->input->post('debtobligation_id');
        if (!empty($debtobligation_id)){
            $gifters_data = $this->Gifts_model->select_debtobligation_gifts($debtobligation_id);
            if ($gifters_data) {
                $giftersData = array();
                foreach ($gifters_data as $gifters) {
                    $user_data = $this->Users_model->select_where($gifters['gift_from']);
                    $giftersData[] = array(
                        "gift_id" => $gifters['gift_id'],
                        "gift_amount" => $gifters['gift_amount']/100,
                        "debtobligationid" => $gifters['debtobligationid'],
                        "gift_mode" => $gifters['gift_mode'],
                        "gift_from" => $gifters['gift_from'],
                        "gift_to" => $gifters['gift_to'],
                        "cc_holdername" => $gifters['cc_holdername'],
                        "cc_number" => $gifters['cc_number'],
                        "cc_expmonth" => $gifters['cc_expmonth'],
                        "date_added" => $gifters['date_added'],
                        "cc_expyear" => $gifters['cc_expyear'],
                        "blessing_message" => $gifters['blessing_message'],
                        "user_name" => $user_data[0]['user_name']
                    );
                }
                $response['status'] = true;  
                $response['data'] = $giftersData;                          
                $response['message'] = 'Gifters Fetched successfully.';
            } else {
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        }else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get User Invite Code
        method    : Post[form-data]
        Parameter : {user_id*}     
        URL       :https://trift.yesitlabs.xyz/apis/get_user_invite_code
        Response  : JSON 
    */
    public function get_user_invite_code(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $userdata = $this->Users_model->select_where($user_id);
            if ($userdata) {
                $data['user_id'] = $userdata[0]['user_id'];
                $data['invite_code'] = $userdata[0]['invite_code'];
                $response['status'] = true;  
                $response['data'] = $data;                                     
                $response['message'] = 'user invite code fetched successfully.';
            } else{
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : invite User.
        method    : Post[form-data]
        Parameter : {user_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/invite_user
        Response  : JSON 
    */
    public function invite_user(){
        $invite_from = $this->input->post('invite_from');
        $invite_to = $this->input->post('invite_to');
        $invite_code = $this->input->post('invite_code');
        $invite_status = $this->input->post('invite_status');
        $date_added = date('Y-m-d');
        if (!empty($invite_from) && !empty($invite_to) && !empty($invite_code)){
            $insert = $this->Invites_model->create(array(
                'invite_from' => $invite_from,
                'invite_to' => $invite_to,
                'invite_code' => $invite_code,
                'invite_status' => $invite_status,
                'date_added' => $date_added
            ));
            if($insert) {
                $response['status'] = true;                                   
                $response['message'] = 'User invited successfully.';
            } else{
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get invitees_list .
        method    : POst[form-data]
        Parameter : {user_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/get_user_invitees_list
        Response  : JSON 
    */
    public function get_user_invitees_list(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $data = $this->Invites_model->select_where($user_id);
            if($data) {
                $response['status'] = true;  
                $response['data'] = $data;                                     
                $response['message'] = 'User invitees list fetched successfully.';
            } else{
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function create_custom_account(){
        $debtobligationid = $this->input->post('debtobligation_id');
        $email = $this->input->post('email');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $business_url = $this->input->post('business_url');
        $ssn_last_4 = $this->input->post('ssn_last_4');
        $day = $this->input->post('day');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $city = $this->input->post('city');
        $line1 = $this->input->post('line1');
        $line2 = $this->input->post('line2');
        $postal_code = $this->input->post('postal_code');
        $state = $this->input->post('state');
        $routingnumber = $this->input->post('routingnumber');
        $accountnumber = $this->input->post('accountnumber');
        $file_front = $this->input->post('file_front');
        $file_back = $this->input->post('file_back');

        $stripe = array(
            "secret_key"      => "sk_test_51HJQhdKAGAMmwwzDE6biVq4QmnQkpk9cqbI5HJ2bsxNNzjeaKHZtKYCE7YpMXroC9lC2rbKuxA701y365HeP7PRM00wgvZEnOn",
            "publishable_key" => "pk_test_51HJQhdKAGAMmwwzDrtpxwEZrYCHJiSp3QvAMcwYp8Dq4R1pFufW0VJQ3mpRicBjF5lPA8MU4xWFTr1amJoEg3y2z00tkKjuw3X"
        );

        require_once APPPATH."third_party/stripe/init.php";
        \Stripe\Stripe::setApiKey($stripe['secret_key']);  
       
        // -----set bank account
        $bank_account = \Stripe\Token::create(
        array( "bank_account" => array(
            "country" =>"US", 
            "currency" => "usd", 
            "routing_number" =>$routingnumber,
            "account_number" =>$accountnumber
        )
        ));
        //print_r($bank_account);

        //----create custom account

        $bank_result = \Stripe\Account::create(array(
            "type" => "custom",
            "country" => "US",
            "email" => $email,
            "business_type"=>"individual",
            "business_profile" => ["url" => $business_url],
            "requested_capabilities"=> ['card_payments', 'transfers'],
            "external_account" => $bank_account->id,
            'tos_acceptance' => ['date' => time(),'ip' => $_SERVER['REMOTE_ADDR']],
            'individual' =>['first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'ssn_last_4' => $ssn_last_4, 'dob' => ['day' => $day, 'month' => $month, 'year' => $year], 'address' => ['city' => $city, 'country' => 'US', 'line1' => $line1, 'line2' => $line2, 'postal_code' =>$postal_code, 'state' => $state], 'verification' =>['document' =>['front' => $file_front, 'back' => $file_back]]],
        ));
        echo $bank_result;
        //,'id_number' => $id_number
        $result = $bank_result->jsonSerialize();
        $stripe_account_id = $result['id'];
        $this->Debtobligation_model->update(array(
            'stripe_account_id' => $stripe_account_id
        ), $debtobligationid);
    }

    /*
        Purpose   : Create Registry .
        method    : POST[form-data]
        Parameter : {registry_name*,registry_description*,registry_end_date*,registry_coverimage*,}     
        URL       : https://trift.yesitlabs.xyz/apis/create_registry
        Response  : JSON 
    */
    public function create_registry(){
        $user_id = $this->input->post('user_id');
        $registry_name = $this->input->post('registry_name');
        $registry_description = $this->input->post('registry_description');
        $registry_end_date = $this->input->post('registry_end_date');
        $registry_code = random_string('alnum',10);
        $tmp_name = $_FILES['registry_coverimage']['tmp_name'];
        $file_name = $_FILES['registry_coverimage']['name'];
        $default_image = 'registry_icon.jpg';
        $registry_status = 1;
        $date_added = date('Y-m-d h:i:s');

        
        if(!empty($user_id) && !empty($registry_name) && !empty($registry_description) && !empty($registry_end_date))
        {
            if(!empty($file_name)){
                $registry_id = $this->Registry_model->create(
                array(
                    "userid" => $user_id,
                    "registry_name" => $registry_name,
                    "registry_description" => $registry_description,
                    "registry_end_date" => date('Y-m-d',strtotime($registry_end_date)),
                    "registry_code" => $registry_code,
                    "registry_coverimage" => $file_name,
                    "registry_status" => $registry_status,
                    "date_added" => $date_added
                ));
                move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
                $i = 1;
                while($i){
                    $debtobligation_title = $this->input->post('debtobligation_title'.$i);
                    $goal_amount = $this->input->post('goal_amount'.$i);
                    $accountname = $this->input->post('accountname'.$i);
                    $routingnumber = $this->input->post('routingnumber'.$i);
                    $accountnumber = $this->input->post('accountnumber'.$i);
                    $debtobligations_status = 1;
                    if(!empty($debtobligation_title) && !empty($goal_amount) && !empty($accountname) && !empty($accountnumber)){
                        $debt_id = $this->Debtobligation_model->create(
                            array(
                                "registryid" => $registry_id,
                                "debtobligation_title" => $debtobligation_title,
                                "goal_amount" => $goal_amount,
                                "accountname" => $accountname,
                                "routingnumber" => $routingnumber,
                                "accountnumber" => $accountnumber,
                                "debtobligations_status" => $debtobligations_status,
                                "date_added" => $date_added
                            ));          
                    }
                    else {
                        $response['status'] = false;                   
                        $response['message'] = 'Please provide required fields.' ; 
                        break;
                    }
                    $i++;
                }
                $response['status'] = true;                     
                $response['message'] = 'Data inserted successfully.' ;
            }
            else {
                $registry_id = $this->Registry_model->create(
                array(
                    "userid" => $user_id,
                    "registry_name" => $registry_name,
                    "registry_description" => $registry_description,
                    "registry_end_date" => date('Y-m-d',strtotime($registry_end_date)),
                    "registry_code" => $registry_code,
                    "registry_coverimage" => $default_image,
                    "registry_status" => $registry_status,
                    "date_added" => $date_added
                ));
                //move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
                $i = 1;
                while($i){
                    $debtobligation_title = $this->input->post('debtobligation_title'.$i);
                    $goal_amount = $this->input->post('goal_amount'.$i);
                    $accountname = $this->input->post('accountname'.$i);
                    $routingnumber = $this->input->post('routingnumber'.$i);
                    $accountnumber = $this->input->post('accountnumber'.$i);
                    $debtobligations_status = 1;
                    if(!empty($debtobligation_title) && !empty($goal_amount) && !empty($accountname) && !empty($accountnumber)){
                        $debt_id = $this->Debtobligation_model->create(
                            array(
                                "registryid" => $registry_id,
                                "debtobligation_title" => $debtobligation_title,
                                "goal_amount" => $goal_amount,
                                "accountname" => $accountname,
                                "routingnumber" => $routingnumber,
                                "accountnumber" => $accountnumber,
                                "debtobligations_status" => $debtobligations_status,
                                "date_added" => $date_added
                            ));  
                              
                    }
                    else {
                        $response['status'] = false;                   
                        $response['message'] = 'Please provide required fields.' ; 
                        break;
                    }

                    $i++;
                }
                $response['status'] = true;                     
                $response['message'] = 'Data inserted successfully.' ;
            }
            
        }
        else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get Content Data
        method    : POst[form-data]
        Parameter : {user_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/get_content
        Response  : JSON 
    */
    public function get_content(){
        //echo date_default_timezone_get();die;
        $data = $this->Users_model->get_content_data();
        if($data) {
            $response['status'] = true;  
            $response['data'] = $data;                                     
            $response['message'] = 'Content fetched successfully.';
        } else{
            $response['status'] = false;                   
            $response['message'] = 'No Data found.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get All Debt Obligations
        Parameter : {registry_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/get_all_debt_obligation
        Response  : JSON 
    */
    public function get_all_debt_obligation(){
        $data = $this->Debtobligation_model->select_dbt_name();
        if ($data) {
            $response['status'] = true;  
            $response['data'] = $data;                                     
            $response['message'] = 'All details fetched successfully.';
        } else{
            $response['status'] = false;                   
            $response['message'] = 'No Data Found.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : GET debt Obligation by id .
        method    : Post[form-data]
        Parameter : {debtobligation_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/get_debt_obligation_by_id
        Response  : JSON 
    */

    public function get_debt_obligation_by_id(){
        $debtobligation_id = $this->input->post('debtobligation_id');
        if (!empty($debtobligation_id)){
            $data = $this->Debtobligation_model->select_where($debtobligation_id);
            if($data) {
                $response['status'] = true;  
                $response['data'] = $data;                                     
                $response['message'] = ' Details.';
            } else {
                $response['status'] = false;                   
                $response['message'] = 'No Data Found.';
            }
        }else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Debt Obligation Delete
        method    : Post[form-data]
        Parameter : {debtobligation_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/delete_debtobligation
        Response  : JSON 
    */
    public function delete_debtobligation(){
        $debtobligation_id = $this->input->post('debtobligation_id');
        if (!empty($debtobligation_id)){
            $this->Debtobligation_model->delete_debtobligation($debtobligation_id);
            $response['status'] = true;                                     
            $response['message'] = 'Debt Obligation deleted successfully.';
        }else{
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Find Registry
        method    : POst[form-data]
        Parameter : {search*,}     
        URL       : https://trift.yesitlabs.xyz/apis/find_registry
        Response  : JSON 
    */
    public function find_registry(){
        $search = $this->input->post('search');
        $user_id = $this->input->post('user_id');
        $user_data = $this->Users_model->read_by_user_name($search);
        if(!empty($user_data)){
            $reg_data = array();
            foreach ($user_data as $user) {
                if($user['user_id'] == $user_id) continue;

                $userData['user_name'] = $user['user_name'];
                $userData['user_profilepic'] = $user['user_profilepic'];
                $registry_data = $this->Registry_model->select_user_registry($user['user_id']);
                //echo $this->db->last_query();
                $reg_data1 = array();
                foreach ($registry_data as $reg) {
                    
                    $reg_data1[] = array(
                        'userid' => $reg['userid'],
                        'registry_id' => $reg['registry_id']
                    );
                }
                $reg_data[] = array(
                    'user_id' => $user['user_id'],
                    'user_name' => $user['user_name'],
                    'user_profilepic' => $user['user_profilepic'],
                    'reg_details' => $reg_data1
                );
            }
        }
        else {
            $registry_data = $this->Registry_model->search_registry($search);
            //echo $this->db->last_query();
            $reg_data = array();
            foreach ($registry_data as $reg) {
                $user_details = $this->Users_model->read_by_user_id($reg['userid']);
                if($reg['userid'] == $user_id) continue;

                $reg_data1 = array();
                $reg_data1[] = array(
                    'userid' => $reg['userid'],
                    'registry_id' => $reg['registry_id']
                );
                $reg_data[] = array(
                    'user_id' => $user_details['user_id'],
                    'user_name' => $user_details['user_name'],
                    'user_profilepic' => $user_details['user_profilepic'],
                    'reg_details' => $reg_data1
                );
            }
        }

        if($reg_data) {
            $response['status'] = true;  
            $response['data'] = $reg_data;                                     
            $response['message'] = 'Data fetched successfully.';
        } else{
            $response['status'] = false;                   
            $response['message'] = 'No Data found.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Send Gift
        method    : POst[form-data]
        Parameter : {user_id*,amount*,token*,receiver_id*,receiver_name*}     
        URL       : https://trift.yesitlabs.xyz/apis/send_gift
        Response  : JSON 
    */
    public function send_gift(){
        $user_id = $this->input->post('user_id');
        $amount = $this->input->post('amount');
        $token = $this->input->post('token');
        $receiver_id = $this->input->post('receiver_id');
        $receiver_name = $this->input->post('receiver_name');
        $debtobligationid = $this->input->post('debtobligation_id');
        $payment_method = $this->input->post('payment_method');
        $cc_holdername = $this->input->post('cc_holdername');
        $cc_number = $this->input->post('cc_number');
        $cc_expmonth = $this->input->post('cc_expmonth');
        $cc_expyear = $this->input->post('cc_expyear');
        $blessing_message = $this->input->post('blessing_message');

        $debt_obli_data = $this->Debtobligation_model->select_where($debtobligationid);
        $stripe = array(
            "secret_key"      => "sk_test_51HJQhdKAGAMmwwzDE6biVq4QmnQkpk9cqbI5HJ2bsxNNzjeaKHZtKYCE7YpMXroC9lC2rbKuxA701y365HeP7PRM00wgvZEnOn",
            "publishable_key" => "pk_test_51HJQhdKAGAMmwwzDrtpxwEZrYCHJiSp3QvAMcwYp8Dq4R1pFufW0VJQ3mpRicBjF5lPA8MU4xWFTr1amJoEg3y2z00tkKjuw3X"
        );

        require_once APPPATH."third_party/stripe/init.php";
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $transfer = \Stripe\Transfer::create([    //Transfer 
          "amount" => $amount * 100,
          "currency" => "usd",
          "destination" => $debt_obli_data[0]['stripe_account_id'],
          "transfer_group" => "registry pay",
        ]);
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $payout = \Stripe\Payout::create([       //payout
            'amount' => $amount * 100,
            'currency' => 'usd',
            ], 
            ['stripe_account' => $debt_obli_data[0]['stripe_account_id'],
        ]);
        $payout = $payout->jsonSerialize();

        /*\Stripe\Stripe::setApiKey($stripe['secret_key']);
        $result = \Stripe\Token::create(
                array(
                    "card" => array(
                        "name" => 'abc',
                        "number" => '4242424242424242',
                        "exp_month" => 11,
                        "exp_year" => 2021,
                        "cvc" => '314'
                    )
                )
            );
        $token = $result['id'];
        add customer to stripe
        $customer = \Stripe\Customer::create([
            'name' => 'Jenny Rosen',
            'email' => "newdev678@yopmail.com",
            'source'  => $token,
            'address' => [
                'line1' => '510 Townsend St',
                'postal_code' => '98140',
                'city' => 'San Francisco',
                'state' => 'CA',
                'country' => 'US',
              ],
            ]);
        $total_amount = $amount * 100;
              
        $charge = \Stripe\Charge::create(array(
            "amount" => $total_amount,
            "currency" => "usd",
            "customer" => $customer->id,
            'description' => "gift",
        )); 
        $chargeJson = $charge->jsonSerialize();*/

        //print_r($chargeJson);die; 
        $insert = $this->Transaction_model->create(
                    array(
                        'userid' => $user_id,
                        'receiver_id' => $receiver_id,
                        'receiver_name' => $receiver_name,
                        'amount' => $total_amount,
                        'token_id' => $payout['id'],
                        'create_date' => date('Y-m-d'),
                        'create_time' => date('H:i:s')
                    ));
        $this->Gifts_model->create(
            array(
                'gift_amount' => $total_amount,
                'debtobligationid' => $debtobligationid,
                'gift_mode' => $payment_method,
                'gift_from' => $user_id,
                'gift_to' => $receiver_id,
                'cc_holdername' => $cc_holdername,
                'cc_number' => $cc_number,
                'cc_expmonth' => $cc_expmonth,
                'cc_expyear' => $cc_expyear,
                'date_added' => date('Y-m-d'),
                'blessing_message' => $blessing_message
            ));
        $transaction_data = $this->Transaction_model->select_transaction_by_id($insert);
        $user_details = $this->Users_model->read_by_user_id($user_id);
        //print_r($transaction_data);
        $data = array(
            'userid' => $user_id,
            'user_name' => $user_details['user_name'],
            'receiver_id' => $receiver_id,
            'receiver_name' => $receiver_name,
            'amount' => $amount,
            'transaction_id' => $payout['id'],
            'payment_status' => $payout['status'],
            'create_date' => date('Y-m-d', strtotime($transaction_data[0]['create_date'])),
            'create_time' => date('h:i A', strtotime($transaction_data[0]['create_time']))
        );
        if($insert){                    
            $response['status'] = true; 
            $response['data'] = $data;                          
            $response['message'] = 'Transaction completed successfully.' ;
        } else {
            $response['status'] = false;                   
            $response['message'] = 'Some problems occurred, please try again.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function paypal_transaction(){
        $user_id = $this->input->post('user_id');
        $amount = $this->input->post('amount');
        $token = $this->input->post('token');
        $receiver_id = $this->input->post('receiver_id');
        $receiver_name = $this->input->post('receiver_name');
        $debtobligationid = $this->input->post('debtobligation_id');
        $payment_method = $this->input->post('payment_method');
        //print_r($_POST);
        require_once APPPATH."vendor/braintree/braintree_php/lib/Braintree.php";
       // print_r(APPPATH."third_party/braintree/Braintree.php");die;
        $gateway = new Braintree_Gateway([
          'environment' => 'sandbox',
          'merchantId' => 'bmynstpd59jsdxnt',
          'publicKey' => 'gw89gjbjg989jzw2',
          'privateKey' => 'cf7dcda528ce406643a9b001c4b52313'
        ]);
        /*$clientToken = $gateway->clientToken()->generate();
        echo json_encode($clientToken);die;*/
        /*$clientToken = $gateway->clientToken()->generate();
       // print_r($clientToken);die;
        $result123 = $gateway->paymentMethodNonce()->create();
        print_r($result123);die;
        $nonce = $result123->paymentMethodNonce->nonce;
        print_r($nonce);die;*/
        $result = $gateway->transaction()->sale([
          'amount' => $amount,
          'paymentMethodNonce' => $token,
          'options' => [
            'submitForSettlement' => True,
            'skipAdvancedFraudChecking' => True
          ]
        ]);
        $total_amount = $amount*100;
        //print_r($result);die;
        if ($result->success) {
            $insert = $this->Transaction_model->create(
                    array(
                        'userid' => $user_id,
                        'receiver_id' => $receiver_id,
                        'receiver_name' => $receiver_name,
                        'amount' => $total_amount,
                        'token_id' => $result->transaction->id,
                        'create_date' => date('Y-m-d'),
                        'create_time' => date('H:i:s')
                    ));
            $this->Gifts_model->create(
                array(
                    'gift_amount' => $total_amount,
                    'debtobligationid' => $debtobligationid,
                    'gift_mode' => $payment_method,
                    'gift_from' => $user_id,
                    'gift_to' => $receiver_id,
                    'cc_holdername' => '',
                    'cc_number' => '',
                    'cc_expmonth' => '',
                    'cc_expyear' => '',
                    'date_added' => date('Y-m-d'),
                    'blessing_message' => ''
                ));
            $transaction_data = $this->Transaction_model->select_transaction_by_id($insert);
            $user_details = $this->Users_model->read_by_user_id($user_id);
            //print_r($transaction_data);
            $data = array(
                'userid' => $user_id,
                'user_name' => $user_details['user_name'],
                'receiver_id' => $receiver_id,
                'receiver_name' => $receiver_name,
                'amount' => $amount,
                'transaction_id' => $result->transaction->id,
                'payment_status' => $result->transaction->status,
                'create_date' => date('Y-m-d', strtotime($transaction_data[0]['create_date'])),
                'create_time' => date('h:i A', strtotime($transaction_data[0]['create_time']))
            );
            $response['status'] = True;                   
            $response['data'] = $data; 
            $response['message'] = 'Transaction completed successfully.' ;
        } 
        else {
            $response['status'] = false;                   
            $response['message'] = $result->message;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Export Transaction Receipt
        Parameter : {user_id*,}     
        URL       : https://trift.yesitlabs.xyz/apis/export_transaction
        Response  : JSON 
    */
    public function export_transaction(){
        $style = APPPATH . 'third_party/pdf/css/style.css';
        include APPPATH . 'third_party/pdf/pdf.php';
        $user_id = $this->input->post("user_id");
        $user_details = $this->Users_model->read_by_user_id($user_id);
        $pdf_data = $this->Transaction_model->select_latest_transaction($user_id);
        if($pdf_data){
            $total_amount = ($pdf_data[0]['amount']/100);
            $html = '<link rel="stylesheet" href="'.$style.'">';
            $html .="
                <h3 style='text-align: center;'>Transaction Details</h3>
                <br/>
                <h4>From: ".$user_details['user_name']."</h4>
                <h4>To: ".$pdf_data[0]['receiver_name']."</h4>
                <h4>Amount: ".$total_amount."</h4>
                <h4>Date: ".date('m-d-Y', strtotime($pdf_data[0]['create_date'])).' '. date('h:i A', strtotime($pdf_data[0]['create_time']))."</h4>
                ";
            $date = date('Y-m-d_H-i-s');
            $dompdf = new Pdf();
            $dompdf->load_html($html);
            $dompdf->render();
            $file_name = FCPATH."assets/transaction_pdf/transaction_".$date.".pdf";
            file_put_contents($file_name, $dompdf->output());
            $response['status'] = true; 
            $response['pdf'] = base_url().'assets/transaction_pdf/transaction_'.$date.".pdf";     
            $response['message'] = 'Data exported.';
        }
        else {
            $response['status'] = false;              
            $response['message'] = 'No Data found.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Save recent search
        Parameter : {search_text*, user_id*}     
        URL       : https://trift.yesitlabs.xyz/apis/save_recent_search
        Response  : JSON 
    */
    public function save_recent_search(){
        $search_text = $this->input->post('search_text');
        $user_id = $this->input->post('user_id');
        $other_user_id = $this->input->post('other_user_id');
        if (!empty($search_text) && !empty($user_id)){
            $insert = $this->Recent_search_model->create(array(
                'search_text' => $search_text,
                'userid' => $user_id,
                'other_user_id' => $other_user_id,
                'date_added' => date('Y-m-d H:i:s')
            ));
            $response['status'] = true;                                   
            $response['message'] = 'Recent search saved successfully.';
        } else {
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get recent search
        Parameter : {user_id*}     
        URL       : https://trift.yesitlabs.xyz/apis/get_recent_search
        Response  : JSON 
    */
    public function get_recent_search(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $search_data = $this->Recent_search_model->select($user_id);
            //echo $this->db->last_query();
            if(!empty($search_data)){
                $search_data_array = array();
                foreach ($search_data as $search) {
                    $user_details = $this->Users_model->read_by_user_id($search['other_user_id']);
                    $search_data_array[] = array(
                        'recent_search_id' => $search['recent_search_id'],
                        'search_text' => $search['search_text'],
                        'date_added' => date('m-d-Y', strtotime($search['date_added'])),
                        'other_user_id' => $search['other_user_id'],
                        'user_profilepic' => $user_details['user_profilepic']
                    );
                }
                $response['status'] = true; 
                $response['data'] = $search_data_array;                     
                $response['message'] = 'Recent search fetched successfully.';
            }
            else {
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else {
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Save referal registration
        Parameter : {user_id*, register_user_id*}     
        URL       : https://trift.yesitlabs.xyz/apis/save_referal
        Response  : JSON 
    */
    public function save_referal(){
        $user_id = $this->input->post('user_id');
        $register_user_id = $this->input->post('register_user_id');
        
        if (!empty($user_id) && !empty($register_user_id)){
            $this->Invites_model->update_invite_referal(array(
                'invite_status' => 3, //3 status for successfully registration
                'date_added' => date('Y-m-d h:i:s')
            ), $user_id, $register_user_id);
            $response['status'] = true;                                   
            $response['message'] = 'Referal code used successfully.';
        } else {
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Referal registration count
        Parameter : {user_id*}     
        URL       : https://trift.yesitlabs.xyz/apis/count_referal
        Response  : JSON 
    */
    public function count_referal(){
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)){
            $data = $this->Invites_model->count_referal($user_id);
            if(!empty($data)){
                $response['status'] = true;   
                $response['data'] = count($data);                                  
                $response['message'] = 'Referal count fetched successfully.';
            }
            else {
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else {
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : Get user data by invite code
        Parameter : {invite_code*}     
        URL       : https://trift.yesitlabs.xyz/apis/user_by_invite_code
        Response  : JSON 
    */
    public function user_by_invite_code(){
        $invite_code = $this->input->post('invite_code');
        if (!empty($invite_code)){
            $data = $this->Users_model->read_by_code($invite_code);
            if(!empty($data)){
                $response['status'] = true; 
                $response['data'] = $data;                                   
                $response['message'] = 'User fetched successfully.';
            }
            else {
                $response['status'] = false;                   
                $response['message'] = 'No Data found.';
            }
        } else {
            $response['status'] = false;                   
            $response['message'] = 'Please provide required fields.';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /*
        Purpose   : logout .
        method    : POST[form-data]
        Parameter : {}     
        URL       : https://trift.yesitlabs.xyz/apis/logout
        Response  : JSON 
    */
    public function logout(){
        //delete all session
        $user_id = $this->input->post("user_id");
        if(session_destroy()){
            $response['status'] = true;                           
            $response['message'] = 'logout successfully.' ;
        }
        header('Content-Type: application/json');
        echo json_encode($response); 
    }

/*********************
Purpose   : Message .
method    : POST[form-data]
Parameter : {message_from*,message_to*,message_text*}     
URL       : https://trift.yesitlabs.xyz/apis/message
Response  : JSON 
*/
public function message(){
    //$data1['id']=$this->input->get('user_id');
    $messageData = array();
    $messageData['message_from']=$this->input->post('message_from');
    $messageData['message_to'] = $this->input->post('message_to');
    $messageData['message_text'] = $this->input->post('message_text');
    $messageData['date_added'] = date('Y-m-d h:i:s');

    //print_r($messageData);die;
    if(!empty($messageData['message_from'])  && !empty($messageData['message_to']) && !empty($messageData['message_text']) ){

        $insert = $this->Message_model->create($messageData);
    //echo $this->db->last_query();die;
        if($insert){                    
            $response['status']=TRUE;                           
            $response['message'] = 'Message has been added successfully.' ;
        }else{
            $response['status']=FALSE;                   
            $response['message'] = 'Some problems occurred, please try again.' ;
        }
    }
    else{
        $response['status']=FALSE;                   
        $response['message'] = 'Provide complete registry information to create.' ;
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}



public function get_message(){

    $data1['id']=$this->input->get('user_id');
//print_r($data1['id']);die;
    if (!empty($data1)){

        $data=$this->Message_model->select_where($data1['id']);
//echo $this->db->last_query();die;
        if ($data) {
            $response['status']=TRUE;  
            $response['data']=$data;                                     
            $response['message'] = ' Details.';
        }else{
            $response['status']=FALSE;                   
            $response['message'] = 'Not Data.';
        }
    }else{
        $response['status']=FALSE;                   
        $response['message'] = 'Provide complete information.';
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

}


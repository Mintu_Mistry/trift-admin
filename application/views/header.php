<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">

	<title>Trift - Admin &amp; Dashboard</title>
	
	<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin="">

	<!-- PICK ONE OF THE STYLES BELOW -->
	<link href="<?php echo base_url();?>css/classic.css" rel="stylesheet"> 
	<!-- <link href="css/corporate.css" rel="stylesheet"> 
	 <link href="css/modern.css" rel="stylesheet">  -->

	<!-- BEGIN SETTINGS -->
	<!-- You can remove this after picking a style -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/jquery.dataTables.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/jquery.dataTables.bootstrap.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/dataTables.buttons.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/buttons.flash.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/buttons.html5.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/buttons.print.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/buttons.colVis.min.js"></script>
	<script src="http://artsfinance.yesitlabs.xyz/js/dataTables.select.min.js"></script>
	<!-- Datatable -->
	<!-- <script src="<?php// echo base_url()?>assets/js/jquery.dataTables.min.js"></script>
	<script src="<?php// echo base_url()?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
	<script src="<?php //echo base_url()?>assets/js/dataTables.buttons.min.js"></script>
	<script src="<?php// echo base_url()?>assets/js/buttons.flash.min.js"></script>
	<script src="<?php// echo base_url()?>assets/js/buttons.html5.min.js"></script>
	<script src="<?php //echo base_url()?>assets/js/buttons.print.min.js"></script>
	<script src="<?php// echo base_url()?>assets/js/buttons.colVis.min.js"></script>
	<script src="<?php// echo base_url()?>assets/js/dataTables.select.min.js"></script>
	 Datatable --> 
		
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	<style>
		body {
			opacity: 0;
		}
		.color{
			background: #47bac1;
			
		}
		.cursor{
			cursor: pointer;
		}
		.isDisabled {
			pointer-events: none;
		  color: currentColor;
		  
		  opacity: 0.9;
		  text-decoration: none;
		}
		.settings {
		    display: none !important;
		}
		.ongoing{
			color: #009933;
			
		}
		.expired{
			color: #cc0000;
		}
		.view{
			color: #0000ff;
		}
		.regdec{
			min-width: 180px;
		}
		.regdate{
			min-width: 65px;
		}
		.minwidth120{
			min-width: 120px;
		}
		.minwidth140{
			min-width: 140px;
		}
		.minwidth180{
			min-width: 180px;
		}
		.minwidth100{
			min-width: 100px;
		}
		.logo{
			height: 38px;
			width:20%;
			align-items: center;
		}
		.max-width{
			display: inline-block;
		    word-break: break-word;
		    min-width: 200px;
		    max-width: 200px;
		}
	</style>
	<script src="<?php echo base_url();?>assets/js/settings.js"></script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>
<body>
	<div class="wrapper">
		<nav id="sidebar" class="sidebar">
			<div class="sidebar-content ">
				<a href="<?php echo base_url('admin');?>" class="sidebar-brand" >
          <!-- <i class="align-middle" data-feather="box"></i> -->
          		<img class="logo" src="<?php echo base_url()?>assets/images/logo.png">
          <span class="align-middle">Trift</span>
        </a>

				<ul class="sidebar-nav">
					
					<li class="sidebar-item active">
						<a href="<?php echo base_url('admin');?>"  class="sidebar-link">
              <i class="align-middle feather-md text-white" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
            </a>
						
					</li>
					<li class="sidebar-item active mt-2">
						<a href="<?php echo base_url('admin');?>/users"  class="sidebar-link">
              <i class="align-middle feather-md text-primary" data-feather="users"></i> <span class="align-middle">Users</span>
            </a>
						
					</li>
					<!-- <li class="sidebar-item active mt-2">
						<a href="invites"  class="sidebar-link">
              <i class="align-middle" data-feather="book-open"></i> <span class="align-middle">Invites</span>
            </a>
						
					</li> -->
					<li class="sidebar-item active mt-2">
						<a href="<?php echo base_url('admin');?>/registry"  class="sidebar-link">
              <i class="align-middle feather-md text-warning" data-feather="book-open"></i> <span class="align-middle">Registries</span>
            </a>
						
					</li>
					<li class="sidebar-item active mt-2">
						<a href="<?php echo base_url('admin');?>/debt_obligation"  class="sidebar-link">
              <i class="align-middle feather-md text-success" data-feather="activity"></i> <span class="align-middle">Debt Obligations</span>
            </a>
						
					</li>
					<li class="sidebar-item active mt-2">
						<a href="<?php echo base_url('admin');?>/gifts"  class="sidebar-link">
              <i class="align-middle feather-md text-danger"  data-feather="gift"></i> <span class="align-middle">Gifts</span>
            </a>
						
					</li>
				</ul>

				

			</div>
		</nav>

		<div class="main">
			<nav class="navbar navbar-expand navbar-light bg-white">
				<a class="sidebar-toggle d-flex mr-2">
          <i class="hamburger align-self-center"></i>
        </a>
				<div class="navbar-collapse collapse">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							<a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-toggle="dropdown">
                <i class="align-middle" data-feather="settings"></i>
              </a>
              
							<a class="nav-link dropdown-toggle d-none d-sm-inline-block"  data-toggle="dropdown">
                <img src="<?php echo base_url();?>assets/images/<?php echo htmlentities($admin['admin_profilepic']);?> " class="avatar img-fluid rounded-circle mr-1" alt="<?php echo $admin['admin_name'];?>"> <span class="text-dark"><?php echo $admin['admin_name'];?></span>
              </a>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="<?php echo base_url();?>admin/profile"><i class="align-middle mr-1" data-feather="user"></i> Profile</a>
								
								<a class="dropdown-item" href="<?php echo base_url();?>admin/logout" ><i data-feather="log-out" class="align-middle mr-1" ></i>   Logout</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>

			<main class="content">

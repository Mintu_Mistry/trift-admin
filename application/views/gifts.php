<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title"><i class="align-middle feather-md text-danger"  data-feather="gift"></i>
										<?php	

									if(isset($debtobligation_id))
									{
										@$giftsdata=$this->Gifts_model->select_where($debtobligation_id)[0];
										//print_r($giftsdata);
										
										?>
										Gifts for
										<?php 
										?><b>
										<?php 
										  $userid= $giftsdata['gift_from'];
										@$userfrom = $this->Users_model->select_where($userid)[0];
										echo $userfrom['user_name'];
										 ?>
										 	
										 </b>
										 <?php
									}
									else{
										?>
										Gifts
										<?php 
									}
									?>

									 </h5>
									<h6 class="card-subtitle text-muted"></h6>
								</div>
								<div class="card-body">
									<table id="datatables-buttons" class="table table-responsive table-striped " style="width:100%">
										<thead>
											<tr>
												<th># </th>
												<th class="minwidth140">Gifts From</th>
												<th class="minwidth140">Gifts To</th>
												<th class="minwidth120"> Gifts Amount</th>
												<th class="minwidth140">Gifts Mode</th>
												<th class="minwidth140">Debt Obligation Name</th>
												<th class="minwidth140">Registry Name</th>
												<th class="minwidth140">Card Holder Name</th>
												<th class="minwidth120">Card Number</th>
												<th class="minwidth100">Card Expiry Date</th>
											</tr>
										</thead>
										<tbody>

										<?php $i=0;foreach ($gifts as $key => $value) {
											$i++;
											$fromid=  $value['gift_from'];
											$toid=$value['gift_to'];
											$debtoblidation_id=$value['debtobligationid'];
											//echo $debtoblidation_id;
											@$debtobligation=$this->Debtobligation_model->select_where($debtoblidation_id,true)[0];
											//print_r($debtobligation);
											 $registryid=$debtobligation['registryid'];
											 //echo $registryid;
											 @$registry=$this->Registry_model->select_where($registryid,true)[0];

										   @$userfrom = $this->Users_model->select_where($fromid,true)[0];
										   @$userto=$this->Users_model->select_where($toid,true)[0];							  
										?>

											<tr>
												<td><?php echo $i;?></td>
												<td><?php echo $userfrom['user_name'];?></td>
												<td><?php echo $userto['user_name'];?></td>
												<td><b>$</b><?php echo number_format( floatval( $value['gift_amount']/100),2);?></td>
												<td><?php echo $value['gift_mode'];?></td>
												
												<td><?php echo $debtobligation['debtobligation_title'];?></td>
												<td><?php echo $registry['registry_name'];?></td>
												<td><?php echo $value['cc_holdername'];?></td>
												<td><?php echo $value['cc_number'];?></td>
												<td><?php echo $value['cc_expmonth']."/". $value['cc_expyear'];?></td>
											</tr>
											<?php }?>
												
										</tbody>
									</table>
								</div>
							</div>
						</div>
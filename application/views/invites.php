<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title"><i class="align-middle" data-feather="users"></i> Invites</h5>
									<h6 class="card-subtitle text-muted"></h6>
								</div>
								
								<div class="card-body">
									<table id="datatables-buttons" class="table table-striped" style="width:100%">
										<thead>
											<tr>
												<th>Invites From</th>
												<th>Invites to</th>
												<th>Action</th>
											</tr>
										</thead>
										<?php foreach ($invites as $key => $value) {
											?>
										<tbody>
											
											<tr>
												<td><?php echo $value['invite_from'];?>
												</td>
												<td><?php echo $value['invite_to'];?></td>
												<td><div class="hidden-sm hidden-xs btn-group">
														<a data-target="#myModal<?php echo 
														$value['invite_id'];?>" id="bt-modal" data-toggle="modal"><i class="align-middle text-primary" data-feather="edit"></i></a>

												<a href=""><i class="align-middle text-danger" data-feather="trash-2" value="<?php echo $value['invite_id'];?>"></i></a>
															
	                                  </div>
	

	                              </td>
											</tr>	
										</tbody>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
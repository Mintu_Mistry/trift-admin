
<style type="text/css">
	.minwidth140{
		min-width: 140px;
	}
</style>
<div class="col-12">
	<div class="card">
		<div class="card-header">
			<h5 class="card-title"><i class="align-middle feather-md text-warning" data-feather="book-open"></i>
				<?php 

				if(isset($userid))
				{
					@$userdata=$this->Users_model->select_where(@$userid)[0];
					?>
					Transactions Of <?php ?><b><?php 
					echo $userdata['user_name'];?>
					</b> <?php
				}
				else{
					?>
					Registries
					<?php 
				}
				?>
			</h5>
			<h6 class="card-subtitle text-muted"></h6>

		</div>

								<!-- a href="<?php //echo base_url('add_registry') ?>">
									<button style="float: right;margin-bottom: inherit;height: 40px;width: 15%;color: white;  background-color:#000033;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Registry</button></a> -->
									<?php if($this->session->flashdata('success'))
									{ ?>	
										<div class="alert alert-success">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<?php echo $this->session->flashdata('success');?>
										</div>
									<?php }?>
									<div class="card-body">
										<table id="datatables-buttons" class="table table-responsive table-striped"style="width:100%">
											<thead>
												<tr>
													<th>#</th>
													<!-- <th>Action</th> -->
													<th class="minwidth140">User Name</th>
													<th class="minwidth140">Receiver Name</th>
													<th class="minwidth140">Amount</th>
													<th class="minwidth140">Create Date</th>
													<th class="minwidth140">Create Time</th>

												</tr>
											</thead>
											<tbody>
												<?php 
												$i=0;
												foreach ($transaction_details as $key => $value) {
													$i++;
													$username=$value['userid'];
													@$username = $this->Users_model->select_where($username,true)[0];
													?>
													<tr>
														<td><?php echo $i;?></td>
														<td><?php echo $username['user_name'];?></td>
														<td><?php echo $value['receiver_name'];?></td>
														<td><?php echo $value['amount'];?></td>
														<td><?php echo date("m-d-yy",strtotime($value['create_date']));?></td>
														<td><?php echo date("h:i A",strtotime($value['create_time']));?></td>
														</tr>	
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
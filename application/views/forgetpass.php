<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">

	<title>Login- Trift - Admin &amp; Dashboard </title>

	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">

	<!-- PICK ONE OF THE STYLES BELOW -->
	<link href="<?php echo base_url();?>css/classic.css" rel="stylesheet"> 

</head>

<body>

<!-- ------------ Modal for Forget Password--------- -->

	<div class="card" id="forgetpas" style="margin: auto; width: 450px; margin-top: 200px;">
		<div class="card-body" >
			<div class="m-sm-4">
				<div class="log_hed text-center mb-4">
					<div class="boderbotm"> FORGOT PASSWORD </div>
				</div>
				<h5 class="text-center"> Please enter email address </h5>
				<div class="m-sm-4">
					<div class="text-center">
						<i class="im im-icon-Male"></i>
						<input class="form-control form-control-lg" type="email" id="email" name="email" placeholder="Enter email" required="">
					</div>								
					<div class="text-center mt-3">
						<button type="button" class="btn btn-lg btn-primary" onclick="forgot_password()">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- ---------------- Modal for OTP ---------------- -->
<div class="modal" data-keyboard="false" data-backdrop="static" id="enterotp" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="text-center"> Enter OTP </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
			</div>
			<div class="modal-body">
				<h5 class="text-center"> Please enter the OTP that has been sent to <br> your given Email </h5>
				<div class="m-sm-4">
					<div class="form-group formicon">
						<input class="form-control form-control-lg" type="text" id="otp" name="otp" placeholder="Enter OTP" required="">
					</div>								
					<div class="text-center mt-3">
						<button type="button" class="btn btn-lg btn-primary" onclick="verify_otp()">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ---------------- Modal for New password ---------------- -->
<div class="modal" data-keyboard="false" data-backdrop="static" id="newpass" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
			</div>
			<div class="modal-body">
				<div class="log_hed text-center mb-4 mt-3">
					
				</div>
				<h5 class="text-center"> Please enter new Passwords </h5>
				
					<div class="m-sm-4">
						<div class="form-group optbox_center mb-4">
							<div class="form-group formicon">
							    <!-- <label for="" class="col-sm-4 col-form-label text-right">New Password</label> -->
							    <div class="new_password">
								    <input type="Password" data-sanitize="trim" class="form-control" name="new_pass" id="new_pass" placeholder="New Passwords" required>
								    <div class="imgicon">
										
									</div>
								    <input type="hidden" class="form-control" name="email" id="input_email" value="admin_trift@yopmail.com" >
							    </div>
							</div>
							<div class="form-group formicon">
							    <!-- <label for="" class="col-sm-4 col-form-label text-right">Confirm Password</label> -->
							    <div class="new_password">
								    <input type="password" class="form-control" name="confirm_pass" id="confirm_pass" placeholder="Confirm Password" required="">
								    <div class="imgicon">
										
									</div>
							    </div>
							</div>
						</div>	
						<div class="text-center mt-3">
							<button type="button" class="btn btn-lg btn-primary" onclick="new_password()">Submit</button>
						</div>
					</div>
				
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets\js\app.js"></script>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
		$(function() {

			toastr.options.preventDuplicates = true;
			
			<?php if($this->session->success_msg){
			?>
				toastr.success("<?php echo $this->session->success_msg; ?>");
			<?php }
				if($this->session->error_msg){
			?>
				toastr.error("<?php echo $this->session->error_msg; ?>");
			<?php } ?>
		});
	</script>
<script type="text/javascript">
	function forgot_password() {
		var email = $('#email').val();
		var emailid = email.trim();
		//alert(emailid);
		if(emailid != ""){
			$.post("<?php echo base_url('admin')?>/forget_password_email",{emailid : emailid}, function(response) {
				if(response == 1) {
					toastr.success("Email sent successfully");
					$("#enterotp").modal('show');
				}
				else if(response == 2) {
					toastr.error("Email doesn't exist");
				}
				else{
					toastr.error("Something went wrong");
				}
				
			});
		}
		else {
			toastr.error("Please enter emailid");
		}
	}

	function verify_otp(){
		// var otp = $('#otp').val();
		var otp = $('#otp').val();
		
		$.get("<?php echo base_url('admin')?>/verify_otp/" + otp, function(response) {
			if(response == 1) {
				toastr.success("OTP verified successfully");
				$("#enterotp").modal('hide');
				$("#newpass").modal('show');
			}
			else if(response == 0) {
				toastr.error("OTP is invalid");
			}
			
		});
	}

	function new_password(){
		var new_pass = $('#new_pass').val();
		var confirm_pass = $('#confirm_pass').val();
		var email = $('#input_email').val();
		if(confirm_pass.length && new_pass.length >= 8 ){
			$.post("<?php echo base_url('admin')?>/new_password/",{new_pass: new_pass, confirm_pass: confirm_pass, email: email}, function(response) {
				if(response == 1) {
					toastr.success("Password changed successfully");
					setTimeout(function(){location.href = "<?php echo base_url('admin/login');?>"}, 1000);
				}
				else if(response == 2) {
					toastr.error("ERROR. New Password and Confirm Password don't match");
				}
				else {
					toastr.error("ERROR. New Password and Old Password can't be same");
				}
				
			});
			
		}
		else {
			toastr.error("Minimum Password character should be 8");
		}
	}
</script>
<div class="col-12">
	<div class="card">
		<div class="card-header">
			<h5 class="card-title"><i class="align-middle feather-md text-warning" data-feather="book-open"></i>
				<?php 

				if(isset($userid))
				{
					@$userdata=$this->Users_model->select_where(@$userid)[0];
					?>
					Registries Of <?php ?><b><?php 
					echo $userdata['user_name'];?>
					</b> <?php
				}
				else{
					?>
					Registries
					<?php 
				}
				?>
			</h5>
			<h6 class="card-subtitle text-muted"></h6>

		</div>

								<!-- a href="<?php //echo base_url('add_registry') ?>">
									<button style="float: right;margin-bottom: inherit;height: 40px;width: 15%;color: white;  background-color:#000033;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Registry</button></a> -->
									<?php if($this->session->flashdata('success'))
									{ ?>	
										<div class="alert alert-success">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<?php echo $this->session->flashdata('success');?>
										</div>
									<?php }?>
									<div class="card-body">
										<table id="datatables-buttons" class="table table-responsive table-striped   "  style="width:100%">
											<thead>
												<tr>
													<th>#</th>
													<th>Action</th>
													<th >Registry Name</th>
													<th>Registry Code</th>
													<th >User Name</th>
													<th>User Email</th>

													<th class="regdec">Registry Description</th>

													<th class="minwidth180">Registry Cover Image</th>
													<th class="regdate">Registry End Date</th>
													<th >Debt Obligations</th>
													<th>Expired</th>
													<th>Status</th>

												</tr>
											</thead>
											<tbody>
												<?php 
												$i=0;
												foreach ($registry as $key => $value) {
													$i++;
													$username=$value['userid'];
													@$username = $this->Users_model->select_where($username,true)[0];
													?>


													<tr>
														<td><?php echo $i;?></td>
														<td>
															<div class="hidden-sm hidden-xs btn-group">
															<!-- <a data-target="#myModal<?php //echo 
															$value['registry_id'];?>" id="bt-modal" data-toggle="modal"><i class="align-middle text-primary" data-feather="edit"></i></a> -->

															<a class="delete_confirm" href="<?php echo base_url('admin');?>/registry_delete/<?php echo $value['registry_id'];?>">
																<i class="align-middle text-danger fa-trash-alt" data-feather="trash-2" ></i>
															</a>				

														</div>
													</td>
												<td><?php echo $value['registry_name'];?>
											</td>
											<td><?php echo $value['registry_code'];?></td>
											<td><?php echo $username['user_name'];?></td>
											<td><?php echo $username['user_email'];?></td>

											<td><?php echo $value['registry_description'];?></td>

											<td>
												<img src="<?php echo base_url();?>/assets/images/<?php echo htmlentities ($value['registry_coverimage']); ?>" style="height:100; width:90%;">
											</td>
											<td><?php echo date('m-d-Y', strtotime($value['registry_end_date']));?></td>
											<td><a class="view" href="<?php echo base_url('admin')?>/debt_obligation/<?php echo $value['registry_id'];?>">view</a></td>
											<td>
												<?php 
												if($this->Registry_model->is_expired($value['registry_id']))
												{
													?>								
													<div class="ongoing"><b>Ongoing</b></div>
													<?php 
												} 
												else
												{
													?>
													<div class="expired"><b>Expired</b></div>
													<?php
												}
												?>
											</td>
											<td class="hidden-480">
												<?php if($this->Registry_model->is_expired($value['registry_id'])){	?>
													<?php if($value['registry_status']==1){ ?>
														<a href="<?php echo base_url('admin') ?>/registry_status/<?php echo $value['registry_id'];?>/<?php echo $value['registry_status'];?>?userid=<?php echo @intval($userid);?>"  class="btn btn-success">
															<span class="cil-contrast btn-icon mr-2"></span> Active</a>
														<?php }

														elseif($value['registry_status']==0){?>

															<a href="<?php echo base_url('admin') ?>/registry_status/<?php echo $value['registry_id'];?>/<?php echo $value['registry_status'];?>?userid=<?php echo @intval($userid);?>"  class="btn btn-danger">
																<span class="cil-contrast btn-icon mr-2"></span> Inactive</a>

															<?php }}
															else{?>

																<a href="<?php echo base_url('admin') ?>/registry_status/<?php echo $value['registry_id'];?>/<?php echo $value['registry_status'];?>?userid=<?php echo @intval($userid);?>"  class="btn btn-danger isDisabled" >
																	<span class="cil-contrast btn-icon mr-2"></span> Inactive</a>												
																<?php }
																?>
															</td>

														</tr>	

													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">

	<title>Login- Trift - Admin &amp; Dashboard </title>

	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">

	<!-- PICK ONE OF THE STYLES BELOW -->
	 <link href="<?php echo base_url();?>css/classic.css" rel="stylesheet"> 
	<!-- <link href="css/corporate.css" rel="stylesheet"> -->
	<!-- <link href="css/modern.css" rel="stylesheet"> -->

	<!-- BEGIN SETTINGS -->
	<!-- You can remove this after picking a style -->
	<style>
		body {

			opacity: 0;
		}
		.settings {
			    display: none;
			}
			.cls{
				margin-left: 10px;
				margin-right: 10px;
			}
	</style>
	<script src="<?php echo base_url();?>assets\js\settings.js"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>

<body>
	<main class="main d-flex w-100">
		<div class="container d-flex flex-column">
			<div class="row h-100">
				<div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
					<div class="d-table-cell align-middle">

						<div class="text-center mt-4">
							<h1 class="h2">Welcome</h1>
							<p class="lead">
								Sign in to your account to continue
							</p>
						</div>

						<div class="card">
							<div class="card-body">
								<div class="m-sm-4">
									<div class="text-center">
										<img src="<?php echo base_url();?>assets\images\<?php echo htmlentities($admin['admin_profilepic']);?>"alt="Chris Wood" class="img-fluid rounded-circle" width="110" height="110">
									</div>
									<form action="" method="post">
										<?php if($this->session->flashdata('error'))
										{ ?>	
										<div class="alert alert-danger">
											 <a href="#" class="close cls" data-dismiss="alert" aria-label="close">&times;</a>
										<?php echo $this->session->flashdata('error');?>
									    </div>
								       <?php }?>
										<div class="form-group">
											<label>Email</label>
											<input class="form-control form-control-lg" type="email" name="email" placeholder="Enter your email" required="" value="<?php  if(isset($_COOKIE['login_email'])) echo $_COOKIE['login_email']; ?>">
										</div>
										<div class="form-group">
											<label>Password</label>
											<input class="form-control form-control-lg" type="password" name="password" placeholder="Enter your password" required="" minlength="8" value="<?php if(isset($_COOKIE['login_password'])) echo $_COOKIE['login_password']; ?>">
											<!-- <small>
            <a href="pages-reset-password.html">Forgot password?</a>
          </small> -->
										</div>
										<div>
											<div class="form-group">
												<input type="checkbox" value="rememberme" name="rememberme" <?php if(isset($_COOKIE['login_email'])) echo "checked";?>>
												<label >Remember me</label>
												<a href="<?php echo base_url('admin/forget_pass')?>" style="margin-left: 250px;"> Forgot Password? </a> 
											</div>
										</div>
										
										<div class="text-center mt-3">
											<!-- <input type="button" name="submit" value="LOGIN" class="btn-primary btn "> -->
											 <button type="submit" class="btn btn-lg btn-primary" name="submit">Sign in</button> 
										</div>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url();?>assets\js\app.js"></script>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
		$(function() {

			toastr.options.preventDuplicates = true;
			
			<?php if($this->session->success_msg){
			?>
				toastr.success("<?php echo $this->session->success_msg; ?>");
			<?php }
				if($this->session->error_msg){
			?>
				toastr.error("<?php echo $this->session->error_msg; ?>");
			<?php } ?>
		});
	</script>
</html>




								<?php if($this->session->flashdata('error'))
										{ ?>	
										 <div class="alert alert-danger " style="width: 70%; align-items: center">
											 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
										<?php echo $this->session->flashdata('error');?>
									    </div> 

								       <?php }?>


				<div class="container-fluid p-0">

					<div class="row">
						<div class="col-lg-3">
						</div>
						<div class="col-12 col-lg-6">
							<div class="card">
								<h5 class="card-header info-color text-white color text-center py-4" >
                                    <strong> Update Profile</strong>
                                          </h5>
								<form method="post" enctype= "multipart/form-data" action="<?php echo base_url('admin');?>/edit_profilesubmit">
									<div class="form-group">
												
												<input type="hidden" class="form-control" name="admin_id" placeholder="Enter username" required="" autocomplete="off" value="<?php echo $profile['admin_id'];?>" >
												
											</div>
								<div class="form-group">
												<label> Name</label>
												<input type="text" class="form-control" name="admin_name" placeholder="Enter Name" required="" autocomplete="off" value="<?php echo $profile['admin_name'];?>" >
												
											</div>
								<div class="form-group">
												<label> Email</label>
												<input type="email" class="form-control" name="admin_email" placeholder="Enter User email" required="" autocomplete="off" value="<?php echo $profile['admin_email'];?>">
												
											</div>
											<center><b>NOTE*:-<h5>If you need to change password. Please enter Old, New and Confirm Password Respectively. Otherwise, Remain leave fields blank </h5></b></center>
								<div class="form-group">
												<label>Old Password</label>
												<input type="password" class="form-control" name="password" placeholder="Enter password" autocomplete="off" minlength="8">
											</div>
											
								<div class="form-group">
												<label> New password</label>
												<input type="password" class="form-control" name="npassword" placeholder="Enter password" autocomplete="off" minlength="8" >
											</div>
								<div class="form-group">
												<label> Confirm password</label>
												<input type="password" class="form-control" name="cpassword" placeholder="Enter password" autocomplete="off" minlength="8" >
											</div>
								
								<div class="form-group">
												<label>Profile </label>
												<input type="file" name="adminprofile" class="form-control" value="<?php echo $profile['admin_profilepic'];?>" accept="image/x-png,image/gif,image/jpeg"  >
												
											</div>
												
											
											<button  type="submit" name="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
							<div class="col-12 col-lg-6">
							
							<div class="col-lg-3">
						</div>

							
<style type="text/css">
	.max_width{
		word-break: break-word;
		min-width: 200px;
		max-width: 200px;
	}
</style>

<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title"><i class="align-middle feather-md text-primary" data-feather="users"></i> Users</h5>
									<h6 class="card-subtitle text-muted"></h6>

									<a href="<?php echo base_url('admin/add_user') ?>">
							<button style="float: right;margin-bottom: inherit;height: 40px;width: 10%;color: white;  background-color:#000033;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add User</button></a>
								</div>
								<?php if($this->session->flashdata('success'))
										{ ?>	
										<div class="alert alert-success">
											 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<?php echo $this->session->flashdata('success');?>
									    </div>
								       <?php }?>

								        <?php if($this->session->flashdata('error2'))
										{ ?>	
										 <div class="alert alert-danger">
											 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
										<?php echo $this->session->flashdata('error2');?>
									    </div> 

								       <?php }?>
								<div class="card-body">
									<table id="datatables-buttons" class="table table-responsive table-striped " style="width:100%">
										<thead>
											<tr>
												<th>#</th>
												<th>Action</th>
												<th class="minwidth100" >Profile</th>
												<th>Name</th>
												<th>Email</th>
												<th class="minwidth100">Address</th>
												<!-- <th class="minwidth100">Contact</th> -->
												<th class="minwidth100">Registries</th>
												<th class="minwidth100">Transactions</th>
												<th>Status</th>
												
											</tr>
										</thead>
										
										<tbody>
											<?php 
											$i = 0;
											foreach ($users as $key => $value) {
												$i++;
											?>
											<tr>
												<td><?php echo $i;?></td>
												<td>
													<div class="hidden-sm hidden-xs btn-group">
														<a href="<?php echo base_url('admin') ?>/edit_user/<?php echo $value['user_id'];?>">
															<i class="align-middle text-primary" data-feather="edit">	
															</i>
														</a>
														<a class="delete_confirm"  href="<?php echo base_url('admin');?>/user_delete/<?php echo $value['user_id'];?>">
			                                    			<i class="align-middle text-danger fa-trash-alt" data-feather="trash-2" ></i>
				                        				</a>				
	                                  				</div>
	                             			 	</td>
												<td>
													<img src="<?php echo base_url();?>assets/images/<?php echo htmlentities ($value['user_profilepic']); ?>" style="height:50; width:100%;">
												</td>
												<td><?php echo $value['user_name'];?></td>
												<td><?php echo $value['user_email'];?></td>
												<td class="max_width"><?php echo $value['user_address'];?></td>
												<!-- <td><?php echo $value['user_contact'];?></td> -->
												<td><a class="view" href="<?php echo base_url('admin')?>/registry/<?php echo $value['user_id'];?>">view</a></td>
												<td>
													<a class="view" href="<?php echo base_url('admin')?>/transactions/<?php echo $value['user_id'];?>">view</a>
												</td>
												<td class="hidden-480">
													<?php if($value['user_status']==1){ ?>
														<a href="<?php echo base_url('admin') ?>/user_status/<?php echo $value['user_id'];?>/<?php echo $value['user_status'];?>"  class="btn btn-success">
															<span class="cil-contrast btn-icon mr-2"></span> Active</a>
													<?php }

													elseif($value['user_status']==0){?>

													<a href="<?php echo base_url('admin') ?>/user_status/<?php echo $value['user_id'];?>/<?php echo $value['user_status'];?>"  class="btn btn-danger">
														<span class="cil-contrast btn-icon mr-2"></span> Inactive</a>
												<?php }?>
												</td>
											</tr>

										<?php }?>	
												
										</tbody>
									
									</table>

								</div>
							</div>
						</div>
						 
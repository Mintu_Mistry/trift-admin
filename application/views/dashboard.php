<?php if($this->session->flashdata('success'))
										{ ?>	
										<div class="alert alert-success">
											 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<?php echo $this->session->flashdata('success');?>
									    </div>
								       <?php }?>
	<div class="container-fluid p-0">

					<div class="row">
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<i class="feather-lg text-primary" data-feather="users"></i>
										</div>
										<a href="<?php echo base_url('admin');?>/users">
										<div class="media-body">
											<h3 class="mb-2">
												<?php echo $users;?>
												
											</h3>
											<div class="mb-0">Users</div>
										</div>
									</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<i class="feather-lg text-warning" data-feather="book-open"></i>
										</div>
										<a href="<?php echo base_url('admin');?>/registry">
										<div class="media-body">
											<h3 class="mb-2"><?php echo $registry;?></h3>
											<div class="mb-0">Registry(ies)</div>
										</div></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<i class="feather-lg text-success" data-feather="activity"></i>
										</div>
										<a href="<?php echo base_url('admin');?>/debt_obligation">
										<div class="media-body">
											<h3 class="mb-2"><?php echo $debt_obligation;?></h3>
											<div class="mb-0">Debt Obligation(s)</div>
										</div></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<i class="feather-lg text-danger" data-feather="gift"></i>
										</div>
										<a href="<?php echo base_url('admin');?>/gifts">
										<div class="media-body">
											<h3 class="mb-2"><?php echo $gifts;?></h3>
											<div class="mb-0">Gifts</div>
										</div></a>
									</div>
								</div>
							</div>
						</div>
						
						
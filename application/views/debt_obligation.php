<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title"><i class="align-middle feather-md text-success" data-feather="activity" ></i> 
										<?php		
									if(isset($registryid))
									{
										@$Registrydata=$this->Registry_model->select_where(@$registryid)[0];
										
										?>
										Debt Obligation Of
										<?php 
										?><b>
										<?php 
										 echo $Registrydata['registry_name'];?>
										 </b>
										 <?php
									}
									else{
										?>
										Debt Obligation
										<?php 
									}
									?>
									</h5>
									<h6 class="card-subtitle text-muted"></h6>
								</div>
								<?php if($this->session->flashdata('success'))
										{ ?>	
										<div class="alert alert-success">
											 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<?php echo $this->session->flashdata('success');?>
									    </div>
								       <?php }?>
								<!-- <a href="<?php //echo base_url('add_debtobligation') ?>">
							<button style="float: right;margin-bottom: inherit;height: 40px;width: 25%;color: white;  background-color:#000033;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Debt Obligation</button></a> -->
								<div class="card-body">
									<table id="datatables-buttons" class="table table-responsive table-striped " style="width:100%">
										<thead>
											<tr>
												<th>#</th>
												<th>Action</th>
												<th class="minwidth140">Debt Obligation Title</th>
												<th class="minwidth140">Registry Name</th>
												<th class="minwidth140">User Name</th>
												<th class="minwidth140">User Email</th>
												
												<th class="minwidth120">Debt Obligation Success Amount</th>
												<th class="minwidth140">Debt Obligation Progress Amount</th>
												<th class="minwidth140">Debt Obligation Account Name</th>
												<th class="minwidth140">Debt Obligation Routing Number</th>
												<th class="minwidth140">Debt Obligation Account Number</th>
												<th>Gifters</th>
												<th>Status</th>
												
											</tr>
										</thead>
											<tbody>
										<?php 
										$i=0;
										foreach ($debt_obligation as $key => $value) {
											$i++;
											$registryid1=$value['registryid'];
										@$progressamount = $this->Gifts_model->get_progress_amt($value['debtobligation_id'])[0];
										
											@$registrydata = $this->Registry_model->select_where($registryid1,true)[0];
												//print_r($registrydata);
											$userid=$registrydata['userid'];
											@$username = $this->Users_model->select_where($userid,true)[0];
											?>
									
											
											<tr>
												<td><?php echo $i;?></td>
												<td>
													<div class="hidden-sm hidden-xs btn-group">
														<!-- <a data-target="#myModal<?php //echo 
														$value['debtobligation_id'];?>" id="bt-modal" data-toggle="modal"><i class="align-middle text-primary" data-feather="edit"></i></a> -->
													<a class="delete_confirm"  href="<?php echo base_url();?>admin/debtobligation_delete/<?php echo $value['debtobligation_id'];?>">
			                                   		<i class="align-middle text-danger fa-trash-alt" data-feather="trash-2" ></i>
				                        			</a>	
	                                  			</div>
	                              			</td>
												<td><?php echo $value['debtobligation_title'];?>
												</td>
												<td><?php echo $registrydata['registry_name'];?></td>
												<td><?php echo $username['user_name'];?></td>
												<td><?php echo $username['user_email'];?></td>
												
												<td><b>$</b><?php echo  number_format(floatval($value['goal_amount']),2) ;?></td>
												<td>
												<b>$</b><?php echo number_format(floatval($progressamount['progress']/100),2)  ;?>
											</td>
												<td><?php echo $value['accountname'];?></td>
												<td><?php echo $value['routingnumber'];?></td>
												<td><?php echo $value['accountnumber'];?></td>
												<td><a class="view" href="<?php echo base_url('admin')?>/gifts/<?php echo $value['debtobligation_id'];?>">view</a></td>
											</td>
												
												<td class="hidden-480">
														<?php if($value['debtobligations_status']==1){ ?>
															<a href="<?php echo base_url('admin') ?>/debtobligation_status/<?php echo $value['debtobligation_id'];?>/<?php echo $value['debtobligations_status'];?>?registryid=<?php echo @intval($registryid);?>"  class="btn btn-success">
																<span class="cil-contrast btn-icon mr-2"></span> Active</a>
														<?php }

														elseif($value['debtobligations_status']==0){?>

														<a href="<?php echo base_url('admin') ?>/debtobligation_status/<?php echo $value['debtobligation_id'];?>/<?php echo $value['debtobligations_status'];?>?registryid=<?php echo @intval($registryid);?>"   class="btn btn-danger">
															<span class="cil-contrast btn-icon mr-2"></span> Inactive</a>
													<?php }?>
													</td>
												
											</tr>	
										
										<?php } ?>
									</tbody>
									</table>
								</div>
							</div>
						</div>
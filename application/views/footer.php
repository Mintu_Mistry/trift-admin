</main>

			<footer class="footer">
				<div class="container-fluid">
					<div class="row text-muted">
						<div class="col-6 text-left">
							<ul class="list-inline">
								<li class="list-inline-item">
									<a class="text-muted" href="#">Support</a>
								</li>
								<li class="list-inline-item">
									<a class="text-muted" href="#">Help Center</a>
								</li>
								<li class="list-inline-item">
									<a class="text-muted" href="#">Privacy</a>
								</li>
								<li class="list-inline-item">
									<a class="text-muted" href="#">Terms of Service</a>
								</li>
							</ul>
						</div>
						<div class="col-6 text-right">
							<p class="mb-0">
								&copy; 2020- Yesitlabs
							</p>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<script src="<?php echo base_url();?>assets\js\app.js"></script>

	
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
	
	 <script>
		$(function() {
			$("#datatables-buttons").DataTable({
				/*"scrollX": true*/
				
				 // pageLength: 6,
				 // lengthChange: false,
				 // bFilter: false,
				 // autoWidth: true
				 
			});
		});
	
		
	</script>   
	
<script>
	/////////////////////smart wizard////////////////////////////////////////////////
		
		// Select2
			$(".select2").each(function() {
				$(this)
					.wrap("<div class=\"position-relative\"></div>")
					.select2({
						placeholder: "Select value",
						dropdownParent: $(this).parent()
					});
			})
/////////////////////////////////////////delete//////////////////////////////////////////////////

	// delete confirmation
$(".delete_confirm").click(function(event){
	if (confirm("Are you sure you want to delete?")) {
		return true;
	}
	else {
		event.preventDefault();
	}
});

//////////////////////////input doesnot take space/////////////////////////////////////////////////
$("input").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});

	</script>


</body>



</html>

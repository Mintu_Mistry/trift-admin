-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ftp.yesitlabs.xyz    Database: yesitzxg_trift
-- ------------------------------------------------------
-- Server version	5.6.41-84.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `admin_id` int(10) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(25) NOT NULL,
  `admin_profilepic` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Admin','admin@yesitlabs.com','admin@123','6713813ffa60c09afb28013bea53020a.jpg');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debt_obligations`
--

DROP TABLE IF EXISTS `debt_obligations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `debt_obligations` (
  `debtobligation_id` int(10) NOT NULL AUTO_INCREMENT,
  `debtobligation_title` varchar(100) NOT NULL,
  `registryid` int(10) NOT NULL,
  `goal_amount` decimal(8,2) NOT NULL,
  `accountname` varchar(100) NOT NULL,
  `routingnumber` varchar(100) NOT NULL,
  `accountnumber` varchar(100) NOT NULL,
  `debtobligations_status` int(1) DEFAULT '1',
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`debtobligation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debt_obligations`
--

LOCK TABLES `debt_obligations` WRITE;
/*!40000 ALTER TABLE `debt_obligations` DISABLE KEYS */;
INSERT INTO `debt_obligations` VALUES (12,'event1',10,200.00,'melo1','369852147','9632587410123789',1,'2020-03-04'),(11,'obligation',9,150.00,'melo','123456789','1478523690123456',2,'2020-03-04'),(6,'event debtobligation',6,4500.00,'miley','1234568','7845123690123456',0,'2020-03-02'),(7,'cocktail',7,1700.00,'hanna','45678917','4785123699601234',1,'2020-03-02'),(10,'cocktail2',7,100.00,'lisa','147852369','1594872630123456',1,'2020-03-04'),(13,'obligation2',13,0.00,'kin','456789123','1478520369035478',1,'2020-03-04'),(14,'cocktail3',15,0.00,'kim','123456708','3697410258741236',1,'2020-03-04'),(15,'event3',7,0.00,'peter','456789123','1478523690123456',1,'2020-03-04'),(16,'obligation5',15,0.00,'hanna','147852369','9632587410123789',1,'2020-03-04'),(17,'event6',9,0.00,'peter','456789123','1594872630123456',2,'2020-03-04'),(18,'event3',7,0.00,'kin','456789123','7845123690123456',2,'2020-03-04'),(19,'obligation1',6,0.00,'melo','45678917','9632587410123789',1,'2020-03-04');
/*!40000 ALTER TABLE `debt_obligations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gifts`
--

DROP TABLE IF EXISTS `gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gifts` (
  `gift_id` int(10) NOT NULL AUTO_INCREMENT,
  `gift_amount` decimal(8,2) NOT NULL,
  `debtobligationid` int(10) NOT NULL,
  `gift_mode` varchar(100) NOT NULL,
  `gift_from` int(10) NOT NULL,
  `gift_to` int(10) NOT NULL,
  `cc_holdername` varchar(100) NOT NULL,
  `cc_number` varchar(20) NOT NULL,
  `cc_expmonth` int(2) NOT NULL,
  `date_added` date DEFAULT NULL,
  `cc_expyear` int(2) NOT NULL,
  PRIMARY KEY (`gift_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gifts`
--

LOCK TABLES `gifts` WRITE;
/*!40000 ALTER TABLE `gifts` DISABLE KEYS */;
INSERT INTO `gifts` VALUES (1,1500.00,13,'paypal',10,13,'Lisa147','1478523690123456',10,'2020-02-25',21),(2,1400.00,15,'paypal',12,13,'lisaa','1478523690453123',2,'2020-02-26',28),(3,100.00,6,'paypal',12,17,'lisa','7879796464512345',11,'2020-03-02',24),(4,1200.00,10,'paypal',13,17,'miley','1478523690123499',3,'0000-00-00',31),(5,1000.00,13,'paypal',17,19,'miley','1478523690123445',1,'0000-00-00',26),(6,900.00,7,'paypal',19,10,'eve','1478523690123436',5,'0000-00-00',29),(7,1200.00,6,'paypal',22,17,'lisaa','1478523690123458',12,'0000-00-00',27),(8,1700.00,15,'paypal',30,10,'adam','7852369012345645',6,'0000-00-00',32);
/*!40000 ALTER TABLE `gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invites`
--

DROP TABLE IF EXISTS `invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invites` (
  `invite_id` int(10) NOT NULL AUTO_INCREMENT,
  `invite_from` int(10) NOT NULL,
  `invite_to` int(10) NOT NULL,
  `invite_status` int(1) NOT NULL DEFAULT '1',
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`invite_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invites`
--

LOCK TABLES `invites` WRITE;
/*!40000 ALTER TABLE `invites` DISABLE KEYS */;
INSERT INTO `invites` VALUES (1,3,4,1,'2020-02-25');
/*!40000 ALTER TABLE `invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `message_id` int(10) NOT NULL AUTO_INCREMENT,
  `message_from` int(10) NOT NULL,
  `message_to` int(10) NOT NULL,
  `message_text` varchar(255) NOT NULL,
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,17,13,'how are u?','2020-03-13');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registries`
--

DROP TABLE IF EXISTS `registries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registries` (
  `registry_id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `registry_name` varchar(100) NOT NULL,
  `registry_description` varchar(255) NOT NULL,
  `registry_end_date` date DEFAULT NULL,
  `registry_code` varchar(50) NOT NULL,
  `registry_coverimage` varchar(100) NOT NULL,
  `registry_status` int(1) DEFAULT '0',
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`registry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registries`
--

LOCK TABLES `registries` WRITE;
/*!40000 ALTER TABLE `registries` DISABLE KEYS */;
INSERT INTO `registries` VALUES (6,10,'decor','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.','2020-03-30','1452jfvb','download.jpg',1,'2020-02-27'),(7,17,'catering','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.','2020-03-31','1452jfvb11','download (2).jpg',0,'2020-02-28'),(33,12,'decor3','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups','2020-03-21','mkobhu','download.jpg',1,'2020-03-04'),(9,13,'Styling','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.','2020-02-29','1452jfvb','photo-1501594274184-e8ac81671c76.jpg',2,'2020-02-28'),(10,15,'styling2','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.','2020-03-24','1452jfvbrer','	\r\ndownload (2).jpg',1,'2020-02-28'),(32,10,'registry1','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups','2020-03-22','4rdefg','photo-1501594274184-e8ac81671c76.jpg',1,'2020-03-03'),(13,19,' decor','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.','2020-02-26','gfbhf546','photo-1530103862676-de8c9debad1d.jpg',1,'2020-02-28'),(15,22,'event','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups','2020-03-31','1452jfvb','dd.jpg',1,'2020-03-02'),(30,17,'decor2','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups','0000-00-00','lEawDp','dd.jpg',1,'2020-03-03'),(34,13,'cater','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups','2020-04-01','ghytds','download (2).jpg',2,'2020-03-04'),(35,15,'event2','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups','2020-03-15','mko90j','photo-1530103862676-de8c9debad1d.jpg',2,'2020-03-04'),(36,30,'event4','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups','2020-03-31','wesd45','download.jpg',2,'2020-03-04');
/*!40000 ALTER TABLE `registries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_contact` varchar(12) NOT NULL,
  `user_address` varchar(100) NOT NULL,
  `user_status` int(1) DEFAULT '1',
  `user_profilepic` varchar(100) NOT NULL,
  `date_added` date NOT NULL,
  `user_password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (10,'Dove caremon ','dovecaremon@gmail.com','4576343524','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ',1,'28acef0acb9e643090fa54eecb0ba0f9.jpg','2020-02-26','dove'),(12,'lisa 2','lisa@gmail.com','7894561230','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ',1,'da42debdcdbcf3da2d3693667fecd8a4.jpg','2020-02-27','Lisa@123'),(13,'billy11','peter@gmail.com','4561230789','abcdefghijklmonbhufheieghfieughbcjhscbfghcsdhfgsbcshhdcbbbbbbbvvvvvvvffhewiutriojgtiojgkrenfhwqgewui',1,'images.png','2020-02-27','peter'),(32,'hrvy','hrvy@gmail.com','7896541230',' u6u6',2,'images.png','0000-00-00','hrvy@123'),(33,'nandani','nandani@yopmail.com','9874563210',' ghorhgoir',2,'1f20a255891919.5997402e1e682.jpg','0000-00-00','nandani@123'),(15,'melo','melo@gmail.com','7894561230','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ',1,'28acef0acb9e643090fa54eecb0ba0f9.jpg','2020-02-27','melo'),(17,'ben','ben@gmail.com','3216547890','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ',1,'3c04d5c919cecc56a9b6bceb2e1f7157.jpg','2020-02-27','ben'),(19,'Melo','melo@gmail.com','7867684512','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ',1,'28acef0acb9e643090fa54eecb0ba0f9.jpg','2020-02-28','melo'),(22,'miley crys','miley@gmail.com','3698521470','Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for p',1,'b8da8da3ffb8647ce7fa6bd743eeb611.jpg','2020-03-02','miley'),(34,'new123','new@gmail.com','7894123230',' ghrjghrjkghddg',1,'b8da8da3ffb8647ce7fa6bd743eeb611.jpg','0000-00-00','new@12345'),(30,'user','user@gmail.com','7894561230',' abcdefghijklmnopqrstuvwxyz',2,'6713813ffa60c09afb28013bea53020a.jpg','0000-00-00','user@123'),(31,'lisa','lisa6677@yopmail.com','5456456456',' Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',1,'1f20a255891919.5997402e1e682.jpg','0000-00-00','lisa@6677'),(48,'billy1','billy@gmail.com','7894561230','abcdefghijklmnopqrstwxyz',1,'images.png','2020-03-16','billy@1234');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-25 10:40:52
